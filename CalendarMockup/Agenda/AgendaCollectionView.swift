//
//  AgendaCollectionView.swift
//  CalendarMockup
//
//  Created on 9/2/18.
//  Copyright © 2018 AgendaMockup. All rights reserved.
//


import Foundation
import UIKit

protocol AgendaDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath, withDate date: Date)
    func agendaScrollingWillBegin()
    func agendaScrollingDidEnd()
}

class AgendaCollectionView: UICollectionView {

    // MARK: - Data Linkage

    // Whenever we set our events list, we use it to populate the
    // eventsMap, which is a dictionary with key being the section of
    // the indexPath for the AgendaCollectionView.  The Value of the
    // eventsMap dictionary is an array of AgendaCellData.  The row
    // portion of the indexPath is used as an index into this array.
    //
    // So basically here we are just separating the various events into
    // buckets by date.
    private(set) var eventMap = [Int:[AgendaCellData]]()
    var events = [AgendaCellData]() {
        didSet {

            eventMap.removeAll()
            for event in events {
                let section = indexPathForDate(event.startTime).section

                // Get existing array entry, if any
                var existingArray = eventMap[section] ?? [AgendaCellData]()
                existingArray.append(event)

                // Update
                eventMap[section] = existingArray
            }
        }
    }
    let headerViewHeight = AppConfig.calendarViewFont.pointSize + 4.0

    var agendaDelegate: AgendaDelegate?

    // MARK: - Date Helpers
    public func dateForIndexPath(_ indexPath: IndexPath) -> Date {
        return AppConfig.dateEpoch.offsetBy(days: indexPath.section)
    }

    public func indexPathForDate(_ date: Date) -> IndexPath {
        let days = Calendar.autoupdatingCurrent.dateComponents([.day], from: AppConfig.dateEpoch, to: date).day!
        let indexPath = IndexPath(row: 0, section: days)

        return indexPath
    }

    private func monthNumberForCurrentSelection() -> Int {
        if let paths = indexPathsForSelectedItems {
            if paths.count > 0 {
                return dateForIndexPath(paths[0]).monthOfYear()
            }
        }

        return 1
    }

    public func scrollToDate(_ date: Date) {
        let target = indexPathForDate(date)
        selectItem(at: target, animated: true, scrollPosition: .top)
    }


    // MARK: - LIFECYCLE
    private func customInit() {

        allowsSelection = true
        allowsMultipleSelection = false

        contentInset = UIEdgeInsets.zero
        
        isPagingEnabled = false

        dataSource = self
        delegate = self
    }

    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        customInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }

}





// MARK: - UICollectionViewDataSource
extension AgendaCollectionView: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return AppConfig.maximumDaysFromEpoch
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard section < AppConfig.maximumDaysFromEpoch else {
            fatalError("section out of bounds")
        }

        if let events = eventMap[section] {
            return  1 + events.count
        } else {
            return 2
        }
    }


    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let date = dateForIndexPath(indexPath)
        let events = eventMap[indexPath.section]

        if indexPath.row == 0 {
            // Row 0 is date header
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AgendaHeaderCollectionViewCell", for: indexPath) as! AgendaHeaderCollectionViewCell
            cell.initCell(date: date)

            return cell

        } else if let events = events {
            let index = indexPath.row - 1 // index into events list
            let cellData = events[index]

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AgendaCollectionViewCell", for: indexPath) as! AgendaCollectionViewCell
            cell.initCell(cellData: cellData)

            return cell

        } else {

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AgendaNoEventsCollectionViewCell", for: indexPath) as! AgendaNoEventsCollectionViewCell
            cell.initCell()

            return cell
        }
    }


}


extension AgendaCollectionView: UICollectionViewDelegateFlowLayout {
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

            if indexPath.row == 0 {
                return CGSize(width: collectionView.frame.width, height: AppConfig.agendaDayHeaderFont.pointSize + 2.0)
            } else {
                let eventsForDate = eventMap[indexPath.section]
                if let eventsForDate = eventsForDate {
                    // Has events

                    let event = eventsForDate[indexPath.row - 1]
                    var height = AppConfig.agendaTitleFont.pointSize * 2 + 4.0
                    if event.images.count > 0 {
                        height += IconImageView.iconImageSize.height + 4.0
                    }
                    if event.location != nil {
                        height += AppConfig.agendaLocationFont.pointSize + 4.0
                    }

                    height = max(height, AppConfig.agendaTitleFont.pointSize * 2 + 8.0)
                    return CGSize(width: collectionView.frame.width, height: height)
                } else {
                    // No events
                    return CGSize(width: collectionView.frame.width, height: AppConfig.agendaNoEventsFont.pointSize + 2.0)
                }
            }
        }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
    }


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat { 
        return 8
    }
}


// MARK: - DELEGATE: UICollectionViewDelegate
extension AgendaCollectionView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selected = indexPathsForSelectedItems ?? [IndexPath]()
        guard selected.count > 0 else {
            return
        }

        agendaDelegate?.collectionView(self, didSelectItemAt: indexPath, withDate: dateForIndexPath(indexPath))
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView,  willDecelerate decelerate: Bool) {
        if !decelerate {
            agendaDelegate?.agendaScrollingDidEnd()
        }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        agendaDelegate?.agendaScrollingDidEnd()
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        agendaDelegate?.agendaScrollingWillBegin()
    }
}
