//
//  AgendaCollectionViewCell.swift
//  CalendarMockup
//
//  Created on 9/2/18.
//  Copyright © 2018 AgendaMockup. All rights reserved.
//


import Foundation
import UIKit

class AgendaCollectionViewCell: UICollectionViewCell {
    private let timeAndLengthLabelSize = CGSize(width: 60.0, height: 16.0)
    private let maxAttendeeIcons = 5


    //
    // MARK: - Properties passed in during initCell()
    //
    private var cellData: AgendaCellData! {
        didSet {
            titleLabel?.text = cellData.title
            locationLabel?.text = cellData.location
            coloredDotImageView?.image = cellData.dotIcon
            eventTimeLabel?.text = cellData.startTime.timeAsString
            eventLengthLabel?.text = cellData.lengthAsString

            // remove old images
            for icon in attendeeIcons ?? [UIImageView]() {
                icon.image = nil
            }

            // Assign images
            var imageCount = 0
            for image in cellData.images {
                if imageCount == maxAttendeeIcons {
                    break
                }
                attendeeIcons?[imageCount].image = image
                imageCount += 1
            }

            layoutIfNeeded()
        }
    }


    //
    // MARK: - UIView References
    //
    private var eventTimeLabel: UILabel?
    private var eventLengthLabel: UILabel?

    private var coloredDotImageView: UIImageView?

    private var stackView: UIStackView?
    private var titleLabel: UILabel?
    private var attendeeIcons: [IconImageView]?
    private var attendeeIconsPane: UIView?
    private var attendeeIconHeightConstraints: [NSLayoutConstraint]?
    private var locationIcon: IconImageView?
    private var locationLabel: UILabel?
    private var locationDetailsPane: UIView?


    private var textForegroundColor = UIColor.red

    // Updates visibility of views based on settings
    private func updateDisplayForCurrentSettings() {
        guard cellData != nil else {
            return
        }

        // Hide unused icons
        for i in 0 ..< (attendeeIcons ?? [UIImageView]()).count {
            if i < cellData?.images.count ?? 0 {
                attendeeIcons?[i].isHidden = false
            } else {
                attendeeIcons?[i].isHidden = true
            }
        }

        // Hide entire pane if no icons
        if cellData?.images.count ?? 0 == 0 {
            attendeeIconsPane?.isHidden = true
        } else {
            attendeeIconsPane?.isHidden = false
        }


        if locationLabel == nil || cellData.location == nil {
            locationDetailsPane?.isHidden = true
        } else {
            locationDetailsPane?.isHidden = false
        }


        setNeedsUpdateConstraints()
        setNeedsLayout()
        updateConstraintsIfNeeded()
        layoutIfNeeded()
    }

    // MARK: - initCell -- cell customization
    public func initCell(cellData: AgendaCellData) {

        let viewToUse = contentView

        self.applyContentViewConstraints()
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        setContentHuggingPriority(.required, for: .vertical)

        if cellData.startTime.occursOnSameDayAs(Date()) {
            // Today!
            textForegroundColor = AppConfig.agendaTodayEventTimeColor
        } else {
            textForegroundColor = AppConfig.agendaDefaultEventTimeColor
        }


        // LEFT COLUMN -- event time and length
        // Time of event
        if eventTimeLabel == nil {
            eventTimeLabel = UILabel()
            eventTimeLabel?.textAlignment = .left
            eventTimeLabel?.font = AppConfig.agendaEventTimeFont
            viewToUse.addSubview(eventTimeLabel!)

            eventTimeLabel?.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                NSLayoutConstraint(item: eventTimeLabel!, attribute: .left, relatedBy: .equal, toItem: viewToUse, attribute: .left, multiplier: 1.00, constant: AgendaView.horizontalInset),
                NSLayoutConstraint(item: eventTimeLabel!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.00, constant: timeAndLengthLabelSize.width),
                NSLayoutConstraint(item: eventTimeLabel!, attribute: .top, relatedBy: .equal, toItem: viewToUse, attribute: .top, multiplier: 1.00, constant: 0.00),
                NSLayoutConstraint(item: eventTimeLabel!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.00, constant: timeAndLengthLabelSize.height)
                ])
        }

        // Length of event
        if eventLengthLabel == nil {
            eventLengthLabel = UILabel()
            eventLengthLabel?.textColor = AppConfig.agendaDefaultEventLengthColor
            eventLengthLabel?.textAlignment = .left
            eventLengthLabel?.font = AppConfig.agendaEventTimeFont
            viewToUse.addSubview(eventLengthLabel!)

            eventLengthLabel?.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                NSLayoutConstraint(item: eventLengthLabel!, attribute: .left, relatedBy: .equal, toItem: eventTimeLabel!, attribute: .left, multiplier: 1.00, constant: 0.00),
                NSLayoutConstraint(item: eventLengthLabel!, attribute: .right, relatedBy: .equal, toItem: eventTimeLabel!, attribute: .right, multiplier: 1.00, constant: 0.00),
                NSLayoutConstraint(item: eventLengthLabel!, attribute: .top, relatedBy: .equal, toItem: eventTimeLabel!, attribute: .bottom, multiplier: 1.00, constant: 2.0),
                NSLayoutConstraint(item: eventLengthLabel!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.00, constant: timeAndLengthLabelSize.height),
                NSLayoutConstraint(item: viewToUse, attribute: .bottom, relatedBy: .greaterThanOrEqual, toItem: eventLengthLabel!, attribute: .bottom, multiplier: 1.00, constant: 0)
                ])
        }

        // MIDDLE COLUMN:  Highlighting (blue circle) for selected cell
        if coloredDotImageView == nil {

            coloredDotImageView = UIImageView()
            coloredDotImageView?.contentMode = .center // no scaling!!
            viewToUse.addSubview(coloredDotImageView!)

            coloredDotImageView?.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                NSLayoutConstraint(item: coloredDotImageView!, attribute: .left, relatedBy: .equal, toItem: eventTimeLabel!, attribute: .right, multiplier: 1.00, constant: 20.0),
                NSLayoutConstraint(item: coloredDotImageView!, attribute: .centerY, relatedBy: .equal, toItem: eventTimeLabel!, attribute: .centerY, multiplier: 1.00, constant: 0.00),
                NSLayoutConstraint(item: coloredDotImageView!, attribute: .width, relatedBy: .equal, toItem: eventTimeLabel!, attribute: .height, multiplier: 1.00, constant: 0.00),
                NSLayoutConstraint(item: coloredDotImageView!, attribute: .height, relatedBy: .equal, toItem: eventTimeLabel!, attribute: .height, multiplier: 1.00, constant: 0.00),
                ])
        }


        // RIGHT COLUMN:  Stack view for right-side items (title, attendee icons, location stuff)
        if stackView == nil {
            stackView = UIStackView()
            stackView?.axis = .vertical
            stackView?.spacing = 4
            stackView?.distribution = .equalSpacing
            stackView?.setContentHuggingPriority(.required, for: .vertical)
            viewToUse.addSubview(stackView!)

            stackView?.translatesAutoresizingMaskIntoConstraints = false

            // mindTheGap tries to set the cell bottom to match the stackview bottom, but with non-required priority, so
            // that we don't end up with a large gap on cells without much in them.
            let mindTheGap = NSLayoutConstraint(item: viewToUse, attribute: .bottom, relatedBy: .equal, toItem: stackView!, attribute: .bottom, multiplier: 1.00, constant: 0.00)
            mindTheGap.priority = .defaultHigh

            NSLayoutConstraint.activate([
                NSLayoutConstraint(item: stackView!, attribute: .left, relatedBy: .equal, toItem: coloredDotImageView!, attribute: .right, multiplier: 1.00, constant: 20),
                NSLayoutConstraint(item: stackView!, attribute: .right, relatedBy: .equal, toItem: viewToUse, attribute: .right, multiplier: 1.00, constant: -AgendaView.horizontalInset),
                NSLayoutConstraint(item: stackView!, attribute: .top, relatedBy: .equal, toItem: eventTimeLabel!, attribute: .top, multiplier: 1.00, constant: 0.00),
                NSLayoutConstraint(item:viewToUse, attribute: .bottom, relatedBy: .greaterThanOrEqual, toItem: stackView!, attribute: .bottom, multiplier: 1.00, constant: 0.00),
                mindTheGap
                ])
        }


        // Title of the event
        if titleLabel == nil {
            titleLabel = UILabel()

            titleLabel?.numberOfLines = 2
            titleLabel?.textColor = AppConfig.agendaTitleTextColor
            titleLabel?.font = AppConfig.agendaTitleFont
            titleLabel?.setContentCompressionResistancePriority(.required, for: .vertical)
            titleLabel?.setContentHuggingPriority(.required, for: .vertical)
            titleLabel?.textAlignment = .left

            stackView?.addArrangedSubview(titleLabel!)

            titleLabel?.translatesAutoresizingMaskIntoConstraints = false
        }

        // View to hold attendee icons
        if attendeeIconsPane == nil {
            attendeeIconsPane = UIView()
            attendeeIconsPane?.translatesAutoresizingMaskIntoConstraints = false
            stackView?.addArrangedSubview(attendeeIconsPane!)
        }

        // Attendee icons
        if attendeeIcons == nil {
            attendeeIcons = [IconImageView]()
            attendeeIconHeightConstraints = [NSLayoutConstraint]()

            var itemToTheLeft: UIImageView!
            for i in 0 ..< maxAttendeeIcons {
                let icon = IconImageView(borderColor: AppConfig.calendarSelectionColor)
                icon.contentMode = .scaleAspectFit
                attendeeIconsPane?.addSubview(icon)

                // Constraints
                icon.translatesAutoresizingMaskIntoConstraints = false
                if i == 0 {
                    NSLayoutConstraint.activate([
                        NSLayoutConstraint(item: icon, attribute: .top, relatedBy: .equal, toItem: attendeeIconsPane!, attribute: .top, multiplier: 1.00, constant: 0),
                        NSLayoutConstraint(item: icon, attribute: .left, relatedBy: .equal, toItem: attendeeIconsPane!, attribute: .left, multiplier: 1.00, constant: 0),
                        NSLayoutConstraint(item: icon, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: attendeeIconsPane!, attribute: .bottom, multiplier: 1.00, constant: 0)
                        ])
                } else {
                    NSLayoutConstraint.activate([
                        NSLayoutConstraint(item: icon, attribute: .top, relatedBy: .equal, toItem: itemToTheLeft, attribute: .top, multiplier: 1.00, constant: 0),
                        NSLayoutConstraint(item: icon, attribute: .left, relatedBy: .equal, toItem: itemToTheLeft, attribute: .right, multiplier: 1.00, constant: 10),
                        NSLayoutConstraint(item: icon, attribute: .bottom, relatedBy: .equal, toItem: itemToTheLeft, attribute: .bottom, multiplier: 1.00, constant: 0)
                        ])
                }

                attendeeIcons?.append(icon)
                itemToTheLeft = icon
            }
            
            NSLayoutConstraint.activate([
                NSLayoutConstraint(item: itemToTheLeft, attribute: .right, relatedBy: .lessThanOrEqual, toItem: attendeeIconsPane!, attribute: .right, multiplier: 1.00, constant: 0)
                ])
        }


        // View to hold location icon & label
        if locationDetailsPane == nil {
            locationDetailsPane = UIView()
            locationDetailsPane?.translatesAutoresizingMaskIntoConstraints = false
            stackView?.addArrangedSubview(locationDetailsPane!)
        }

        // Location icon
        if locationIcon == nil {
            locationIcon = IconImageView(size: CGSize(width: 15, height: 15), rounded: false, borderColor: UIColor.clear)
            locationIcon?.image = UIImage(named: "LocationIcon")

            locationIcon?.contentMode = .scaleAspectFit
            locationIcon?.layer.cornerRadius = 0
            locationIcon?.clipsToBounds = true
            locationIcon?.layer.borderWidth = 0
            locationDetailsPane?.addSubview(locationIcon!)

            locationIcon?.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                NSLayoutConstraint(item: locationIcon!, attribute: .top, relatedBy: .equal, toItem: locationDetailsPane!, attribute: .top, multiplier: 1.00, constant: 0),
                NSLayoutConstraint(item: locationIcon!, attribute: .left, relatedBy: .equal, toItem: locationDetailsPane!, attribute: .left, multiplier: 1.00, constant: 0),
                NSLayoutConstraint(item: locationIcon!, attribute: .height, relatedBy: .equal, toItem: locationIcon!, attribute: .width, multiplier: 1.00, constant: 0),
                ])
        }

        // Location label
        if locationLabel == nil {
            locationLabel = UILabel()

            locationLabel?.numberOfLines = 2
            locationLabel?.textColor = AppConfig.agendaDefaultEventTimeColor
            locationLabel?.font = AppConfig.agendaLocationFont
            locationLabel?.setContentCompressionResistancePriority(.required, for: .vertical)
            locationLabel?.setContentHuggingPriority(.required, for: .vertical)
            locationLabel?.textAlignment = .left
            locationDetailsPane?.addSubview(locationLabel!)

            locationLabel?.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                NSLayoutConstraint(item: locationLabel!, attribute: .top, relatedBy: .equal, toItem: locationDetailsPane!, attribute: .top, multiplier: 1.00, constant: 0),
                NSLayoutConstraint(item: locationLabel!, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: locationDetailsPane!, attribute: .bottom, multiplier: 1.00, constant: 0),
                NSLayoutConstraint(item: locationLabel!, attribute: .left, relatedBy: .equal, toItem: locationIcon, attribute: .right, multiplier: 1.00, constant: 4),
                NSLayoutConstraint(item: locationLabel!, attribute: .right, relatedBy: .equal, toItem: locationDetailsPane!, attribute: .right, multiplier: 1.00, constant: 0)
            ])

        }


        // Store the properties we were given
        eventTimeLabel?.textColor = textForegroundColor
        self.cellData = cellData


        // Will show/hide as needed
        updateDisplayForCurrentSettings()
    }


    // MARK: - LIFECYCLE
    private func customInit() {
        layer.backgroundColor = UIColor.clear.cgColor
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }
}

