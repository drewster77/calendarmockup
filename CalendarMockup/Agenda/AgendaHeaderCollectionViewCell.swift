//
//  AgendaHeaderCollectionViewCell.swift
//  CalendarMockup
//
//  Created on 9/3/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import Foundation
import UIKit


// AgendaHeaderCollectionViewCell
//
// Some astute developer may wonder why this is implemented as a UICollectionViewCell,
// rather than a UICollectionReusableView, as is normally used for headers and footers.
//
// The reason for this is that I wanted to be able to scroll the view to the header of
// a given section, rather than to the first cell in the section.
//
// row 0 for each section in the AgendaCollectionView is used for this header view.
class AgendaHeaderCollectionViewCell: UICollectionViewCell {

    // UIView References
    private var label: AgendaCellLabel?
    
    private var isToday = false

    private var intrinsicContentSizeToUse: CGSize?
    override var intrinsicContentSize: CGSize {
        return intrinsicContentSizeToUse ?? super.intrinsicContentSize
    }


    // MARK: - Cell customizations
    func initCell(date: Date) {

        applyContentViewConstraints()
        contentView.translatesAutoresizingMaskIntoConstraints = false
        intrinsicContentSizeToUse = CGSize(width: UIViewNoIntrinsicMetric, height: AppConfig.agendaDayHeaderFont.pointSize + 2.0)
        setContentHuggingPriority(.required, for: .vertical)

        let viewToUse = contentView

        // Figure out title with proper formatting
        var title = ""
        let now = Date()
        isToday = false
        if date.occursOnSameDayAs(now) {
            title = "Today • "
            isToday = true
        } else if date.occursOnSameDayAs(now.offsetBy(days: 1)) {
            title = "Tomorrow • "
        } else if date.occursOnSameDayAs(now.offsetBy(days: -1)) {
            title = "Yesterday • "
        }

        let fmt = DateFormatter()

        if date.monthOfYear() == 1 && date.dayOfMonth() == 1 {
            // January 1st we also include the year
            fmt.dateFormat = "EEEE, MMMM d, yyyy"
            title += fmt.string(from: date)
        } else {
            fmt.dateFormat = "EEEE, MMMM d"
            title += fmt.string(from: date)
        }

        // Set up label view
        if label == nil {
            label = AgendaCellLabel(size: CGSize(width: frame.width - (2 * AgendaView.horizontalInset), height: AppConfig.agendaDayHeaderFont.pointSize))
            label?.numberOfLines = 1
            label?.textAlignment = .left
            label?.font = AppConfig.agendaDayHeaderFont
            viewToUse.addSubview(label!)

            label?.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                NSLayoutConstraint(item: label!, attribute: .left, relatedBy: .equal, toItem: viewToUse, attribute: .left, multiplier: 1.00, constant: AgendaView.horizontalInset),
                NSLayoutConstraint(item: label!, attribute: .right, relatedBy: .equal, toItem: viewToUse, attribute: .right, multiplier: 1.00, constant: -AgendaView.horizontalInset),
                NSLayoutConstraint(item: label!, attribute: .top, relatedBy: .equal, toItem: viewToUse, attribute: .top, multiplier: 1.00, constant: 0.00),
                NSLayoutConstraint(item: label!, attribute: .bottom, relatedBy: .equal, toItem: viewToUse, attribute: .bottom, multiplier: 1.00, constant: 0.00)
                ])
        }
        label?.text = title

        setNeedsUpdateConstraints()
        setNeedsLayout()
        updateConstraintsIfNeeded()
        layoutIfNeeded()

        // Color it up
        if isToday {
            label?.textColor = AppConfig.agendaTodayHeaderTextColor
            backgroundColor = AppConfig.agendaTodayHeaderBackgroundColor
        } else {
            label?.textColor = AppConfig.agendaDefaultHeaderTextColor
            backgroundColor = AppConfig.agendaDefaultHeaderBackgroundColor
        }
    }

}
