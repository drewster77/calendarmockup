//
//  AgendaNoEventsCollectionViewCell.swift
//  CalendarMockup
//
//  Created on 9/2/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//


import Foundation
import UIKit

class AgendaNoEventsCollectionViewCell: UICollectionViewCell {


    //
    // MARK: - UIView References
    //
    private var label: UILabel?

    private var intrinsicContentSizeToUse: CGSize?
    override var intrinsicContentSize: CGSize {
        return intrinsicContentSizeToUse ?? super.intrinsicContentSize
    }

    
    // MARK: - initCell -- cell customization
    public func initCell() {

        contentView.translatesAutoresizingMaskIntoConstraints = false
        intrinsicContentSizeToUse = CGSize(width: UIViewNoIntrinsicMetric, height: AppConfig.agendaNoEventsFont.pointSize + 2.0)
        invalidateIntrinsicContentSize()

        let viewToUse = contentView

        let stack = UIStackView()
        stack.distribution = .equalSpacing
        stack.axis = .vertical
        stack.spacing = 4
        stack.setContentHuggingPriority(.required, for: .vertical)
        viewToUse.addSubview(stack)

        stack.translatesAutoresizingMaskIntoConstraints = false


        // mindTheGap tries to set the cell bottom to match the stackview bottom, but with non-required priority, so
        // that we don't end up with a large gap on cells without much in them.
        let mindTheGap = NSLayoutConstraint(item: viewToUse, attribute: .bottom, relatedBy: .equal, toItem: stack, attribute: .bottom, multiplier: 1.00, constant: 0.00)
        mindTheGap.priority = .defaultLow

        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: stack, attribute: .left, relatedBy: .equal, toItem: viewToUse, attribute: .left, multiplier: 1.00, constant: AgendaView.horizontalInset),
            NSLayoutConstraint(item: stack, attribute: .right, relatedBy: .equal, toItem: viewToUse, attribute: .right, multiplier: 1.00, constant: -AgendaView.horizontalInset),
            NSLayoutConstraint(item: stack, attribute: .top, relatedBy: .equal, toItem: viewToUse, attribute: .top, multiplier: 1.00, constant: 0.00),
            NSLayoutConstraint(item:viewToUse, attribute: .bottom, relatedBy: .greaterThanOrEqual, toItem: stack, attribute: .bottom, multiplier: 1.00, constant: 0.00),
            mindTheGap
            ])

        label = UILabel()
        label?.textColor = AppConfig.agendaNoEventsTextColor
        label?.textAlignment = .left
        label?.numberOfLines = 1
        label?.font = AppConfig.agendaNoEventsFont
        label?.text = "No events"
        stack.addArrangedSubview(label!)

        label?.translatesAutoresizingMaskIntoConstraints = false

        setNeedsUpdateConstraints()
        setNeedsLayout()
        updateConstraintsIfNeeded()
        layoutIfNeeded()
    }


    // MARK: - LIFECYCLE
    private func customInit() {
        layer.backgroundColor = UIColor.clear.cgColor
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }
}
