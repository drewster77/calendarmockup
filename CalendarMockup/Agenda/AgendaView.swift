//
//  AgendaView.swift
//  CalendarMockup
//
//  Created on 8/31/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import Foundation
import UIKit

protocol AgendaViewDelegate {
    func agendaDidSelectDate(_ date: Date, viaScrolling: Bool)
    func agendaScrollingWillBegin()
    func agendaScrollingDidEnd()

}

class AgendaView: UIView {


    // MARK: - DATA LINKAGE & PUBLIC INTERFACE
    private var _cellData = [AgendaCellData]()
    var cellData : [AgendaCellData] {
        get {
            return _cellData
        }
        set {
            _cellData = newValue.sorted(by: { (a, b) -> Bool in
                a.startTime < b.startTime
            })

            collectionView?.events = cellData
        }
    }

    var agendaViewDelegate: AgendaViewDelegate?
    static public let horizontalInset: CGFloat = 20

    public func selectItemWithDate(_ date: Date) {
        collectionView?.scrollToDate(date)
    }

    public func reloadData() {
        collectionView?.reloadData()
    }


    // MARK: - PRIVATE PROPERTIES
    private var collectionView: AgendaCollectionView?


    // scrolledPositionDidUpdate is called by displayLink whenever the user is scrolling
    // the AgendaCollectionView.  We pass this to the delegate, which will determine the
    // indexPath at the top of the screen, and use it to select the corresponding indexPath
    // in the CalendarCollectionView
    private var displayLink: CADisplayLink?
    @objc func scrolledPositionDidUpdate(_ link: CADisplayLink) {
        if let date = getDateForCurrentScrollPosition() {
            agendaViewDelegate?.agendaDidSelectDate(date, viaScrolling: true)
        }
    }
    private func getDateForCurrentScrollPosition() -> Date? {
        guard let collectionView = collectionView else {
            print ("CollectionView is nil")
            return nil }

        let index = collectionView.indexPathsForVisibleItems.reduce(IndexPath(row: Int.max, section: Int.max)) { (highestSoFar, thisOne) -> IndexPath in
            if thisOne < highestSoFar {
                return thisOne
            } else {
                return highestSoFar
            }
        }

        return collectionView.dateForIndexPath(index)
    }

    // Collection View
    private func setupCollectionView() {
        collectionView = AgendaCollectionView(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height), collectionViewLayout: UICollectionViewFlowLayout())
        collectionView?.agendaDelegate = self

        collectionView?.register(AgendaHeaderCollectionViewCell.self, forCellWithReuseIdentifier: "AgendaHeaderCollectionViewCell")
        collectionView?.register(AgendaNoEventsCollectionViewCell.self, forCellWithReuseIdentifier: "AgendaNoEventsCollectionViewCell")
        collectionView?.register(AgendaCollectionViewCell.self, forCellWithReuseIdentifier: "AgendaCollectionViewCell")
        collectionView?.backgroundColor = UIColor.white

        addSubview(collectionView!)

        // Fit to remaining space
        collectionView?.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: collectionView!, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.00, constant: 0.00),
            NSLayoutConstraint(item: collectionView!, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.00, constant: 0.00),
            NSLayoutConstraint(item: collectionView!, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.00, constant: 0.00),
            NSLayoutConstraint(item: collectionView!, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.00, constant: 0.00)
            ])

        layoutIfNeeded()
    }

    // View setup - run once
    private func setupSubviews() {
        if collectionView == nil {
            setupCollectionView()
        }
    }

    // MARK: - LIFECYCLE
    private func customInit() {
        layer.borderWidth = 0
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        setupSubviews()
    }
}


// MARK: - DELEGATE: AgendaDelegate
extension AgendaView: AgendaDelegate {
    func agendaScrollingWillBegin() {
        agendaViewDelegate?.agendaScrollingWillBegin()

        if displayLink == nil {
            displayLink = CADisplayLink(target: self, selector: #selector(scrolledPositionDidUpdate(_:)))
            displayLink?.preferredFramesPerSecond = 60
        }
        displayLink?.isPaused = false
        displayLink?.add(to: .current, forMode: .commonModes)
    }

    func agendaScrollingDidEnd() {
        agendaViewDelegate?.agendaScrollingDidEnd()

        displayLink?.isPaused = true
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath, withDate date: Date) {
        agendaViewDelegate?.agendaDidSelectDate(date, viaScrolling: false)
    }

}

