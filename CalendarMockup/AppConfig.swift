//
//  AppConfig.swift
//  CalendarMockup
//
//  Created on 9/1/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import Foundation
import UIKit

class AppConfig {
    // MARK: - CalendarView items

    // CalendarView -- colors
    static let calendarNormalTextColor = UIColor(named: "CalendarNormalTextColor")!
    static let calendarNormalBackgroundColor = UIColor(named: "CalendarNormalBackgroundColor")!
    static let calendarHighlightBackgroundColor = UIColor(named: "CalendarHighlightBackgroundColor")!
    static let calendarSelectionColor = UIColor(named: "CalendarSelectionColor")!
    static let calendarSelectionTextColor = UIColor(named: "CalendarSelectionTextColor")!
    static let calendarRowSeparatorColor = UIColor(named: "CalendarRowSeparatorColor")!
    static let calendarMonthOverlayColor = UIColor(named: "CalendarMonthOverlayColor")!

    // CalendarView -- fonts
    static let calendarViewFont = UIFont.systemFont(ofSize: 20.0)
    static let calendarSubtitleFont = UIFont.systemFont(ofSize: 16.0)
    static let calendarTitleFont = UIFont.systemFont(ofSize: 15.0)
    static let monthTitleLabelFont = UIFont.systemFont(ofSize: 20.0, weight: .medium)

    // MARK: - AgendaView items

    // AgendaView -- colors
    static let agendaDefaultHeaderTextColor = UIColor(named: "CalendarNormalTextColor")!
    static let agendaDefaultHeaderBackgroundColor = UIColor(named: "CalendarHighlightBackgroundColor")!
    static let agendaTodayHeaderTextColor = UIColor(named: "CalendarSelectionColor")!
    static let agendaTodayHeaderBackgroundColor = UIColor(named: "AgendaTodayHeaderBackgroundColor")!
    static let agendaDefaultEventTimeColor = UIColor(named: "AgendaTitleColor")!
    static let agendaDefaultEventLengthColor = UIColor(named: "CalendarNormalTextColor")!
    static let agendaTodayEventTimeColor = UIColor(named: "CalendarSelectionColor")!
    static let agendaTitleTextColor = UIColor(named: "AgendaTitleColor")!
    static let agendaNoEventsTextColor = UIColor(named: "CalendarNormalTextColor")!

    // AgendaView -- fonts
    static let agendaNoEventsFont = UIFont.systemFont(ofSize: 16.0)
    static let agendaEventTimeFont = UIFont.systemFont(ofSize: 12.0)
    static let agendaTitleFont = UIFont.systemFont(ofSize: 16.0)
    static let agendaLocationFont = UIFont.systemFont(ofSize: 12.0)
    static let agendaDayHeaderFont = UIFont.systemFont(ofSize: 20.0)

    // MARK: - Date range config
    //
    // The dateEpoch is the earliest date the calendar can display, and maximumDaysFromEpoch
    // is the total number of days that can be displayed.
    //
    // In a complete app, these could be broader or perhaps dynamic.  It might make sense to
    // limit the number of events in the past.  For example, if the app limited past events
    // to a maximum of 1 year old, we could dynamically set the dateEpoch to the current date
    // minus 1 year at app launch, and then schedule out some reasonable amount of time into
    // the future.
    //
    // It would be helpful if the user interface that allows the creation of calendar events
    // would limit the creation of events to the same allowable timeframes.
    static let dateEpoch = Date.init(year: 2017, month: 1, day: 1)
    static let maximumDaysFromEpoch = 5000      // about 13.68 years
}
