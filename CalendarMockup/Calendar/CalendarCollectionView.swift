//
//  CalendarCollectionView.swift
//  CalendarMockup
//
//  Created on 9/1/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import Foundation
import UIKit



// MARK: - CalendarDelegate protocol
//
// Protocol for passing some relevant actions up the chain
protocol CalendarDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath, withDate date: Date)
    func calendarScrollingWillBegin()
    func calendarScrollingDidEnd()
}


// MARK: - CalendarCollectionView class
class CalendarCollectionView: UICollectionView {

    var calendarDelegate: CalendarDelegate?

    // We generate eventMap from cellData, which is a simple dictionary
    // mapping indexPath row indices to a flag which indicates if the
    // cell should be highlighted.
    //
    // This is the highlighting that is used when we highlight the full
    // currently display month.
    private(set) var eventMap = [Int:Bool]()
    var cellData = [CalendarCellData]() {
        didSet {

            eventMap.removeAll()
            for event in cellData {
                let row = indexPathForDate(event.startTime).row
                eventMap[row] = true
            }
        }
    }
    
    // MARK - Date Helpers
    public func dateForIndexPath(_ indexPath: IndexPath) -> Date {
        return AppConfig.dateEpoch.offsetBy(days: indexPath.row)
    }

    public func indexPathForDate(_ date: Date) -> IndexPath {

        let days = Calendar.autoupdatingCurrent.dateComponents([.day], from: AppConfig.dateEpoch, to: date).day!
        let indexPath = IndexPath(row: days, section: 0)

        return indexPath
    }


    private func monthNumberForCurrentSelection() -> Int {
        if let paths = indexPathsForSelectedItems {
            if paths.count > 0 {
                return dateForIndexPath(paths[0]).monthOfYear()
            }
        }

        return 1
    }

    // OverlayMode dims the general cell contents and shows the name
    // of the month over the middle of the month of cells.  You can
    // see this when scrolling the CalendarView (top part of the screen).
    private var isOverlayModeActive: Bool = false {
        didSet {
            guard isOverlayModeActive != oldValue else { return }

            for cell in visibleCells {
                if let cell = cell as? CalendarCollectionViewCell {
                    cell.showMonthNameOverlay(isOverlayModeActive, animated: true)
                }
            }
        }
    }


    public func scrollToDate(_ date: Date, animated: Bool) {
        let target = indexPathForDate(date)
        selectItem(at: target, animated: animated, scrollPosition: .top)
    }

    
    // MARK: - LIFECYCLE
    private func customInit() {
        allowsSelection = true
        allowsMultipleSelection = false

        isPagingEnabled = false

        dataSource = self
        delegate = self
    }

    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        customInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }

}





// MARK: - UICollectionViewDataSource
extension CalendarCollectionView: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard section == 0 else {
            fatalError("section out of bounds")
        }

        return AppConfig.maximumDaysFromEpoch
    }


    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {


        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CalendarCollectionViewCell", for: indexPath) as! CalendarCollectionViewCell

        let date = dateForIndexPath(indexPath)


        let day = date.dayOfMonth()
        let month = date.monthOfYear()
        let monthAbbrev = MonthOfTheYear(rawValue: month)?.abbreviation
        let yearText = day == 1 && month == 1 ? "\(date.dateComponents().year!)" : nil
        var monthName = MonthOfTheYear(rawValue: month)!.name
        if month == 1 {
            monthName = "\(monthName) \(date.dateComponents().year!)"
        }
        let dayOfWeek = date.dayOfWeek()
        let isCellBackgroundShaded = month == monthNumberForCurrentSelection()

        let row = indexPath.row
        var showGrayDot = false
        if let grayDotResult = eventMap[row] {
            showGrayDot = grayDotResult
        }


        cell.initCell(cellData: CalendarCellData(startTime: date, dayOfMonth: day, monthAbbreviation: monthAbbrev, yearText: yearText, showGrayDot: showGrayDot, isCellBackgroundShaded: isCellBackgroundShaded, monthName: monthName, dayOfWeek: dayOfWeek), isOverlayModeActive: isOverlayModeActive)

        return cell
    }
}


// MARK: - DELEGATE: UICollectionViewDelegateFlowLayout
extension CalendarCollectionView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = CalendarView.getCellWidthForViewWithWidth(frame.width)
        let height = CalendarView.getCellHeightForViewWithWidth(frame.width)
        return CGSize(width: width, height: height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}


// MARK: - DELEGATE: UICollectionViewDelegate
extension CalendarCollectionView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        calendarDelegate?.collectionView(collectionView, didSelectItemAt: indexPath, withDate: dateForIndexPath(indexPath))

        let selected = indexPathsForSelectedItems ?? [IndexPath]()
        guard selected.count > 0 else {
            return
        }


        // Set background shading based on currently-selected cell
        for cell in visibleCells {
            guard let cell = cell as? CalendarCollectionViewCell else {
                return
            }

            if let path = self.indexPath(for: cell) {
                let month = dateForIndexPath(path).monthOfYear()
                cell.isCellBackgroundShaded = month == monthNumberForCurrentSelection()
            }

        }
    }


    // Watch scrolling so we can activate/deactivate the month-name overlay
    func scrollViewDidEndDragging(_ scrollView: UIScrollView,  willDecelerate decelerate: Bool) {
        isOverlayModeActive = decelerate

        if !decelerate {
            calendarDelegate?.calendarScrollingDidEnd()
        }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        isOverlayModeActive = false
        calendarDelegate?.calendarScrollingDidEnd()
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isOverlayModeActive = true
        calendarDelegate?.calendarScrollingWillBegin()
    }

}
