//
//  CalendarCollectionViewCell.swift
//  CalendarMockup
//
//  Created on 9/1/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import Foundation
import UIKit

class CalendarCollectionViewCell: UICollectionViewCell {

    //
    // MARK: - Properties passed in during initCell()
    //
    private(set) var dayOfMonth: Int = 1 {
        didSet {
            dayOfMonthLabel?.text = "\(dayOfMonth)"
            updateDisplayForCurrentSettings()
        }
    }
    private(set) var monthAbbreviation: String? {
        didSet {
            monthAbbreviationLabel?.text = monthAbbreviation
            updateDisplayForCurrentSettings()
        }
    }

    private(set) var yearText: String? {
        didSet {
            yearTextLabel?.text = yearText
            updateDisplayForCurrentSettings()
        }
    }

    public var showGrayDot: Bool = false {
        didSet {
            updateDisplayForCurrentSettings()
        }
    }

    private(set) var dayOfWeek: Int = 1

    public var monthNameOverlayText: String? {
        didSet {
            monthNameOverlayLabel?.text = monthNameOverlayText
            updateDisplayForCurrentSettings()
        }
    }

    // Background shading for cell -- current month is light gray.  Else white
    public var isCellBackgroundShaded: Bool = false {
        didSet {
            layer.backgroundColor = isCellBackgroundShaded ? AppConfig.calendarHighlightBackgroundColor.cgColor : AppConfig.calendarNormalBackgroundColor.cgColor
        }
    }

    // MARK: - Month Name Overlay
    //
    // When this property is set to true, we animate some changes:
    //   1- The alpha for each visible cell is reduced to 0.50 (50%)
    //   2- The alpha for the overlay text is raised to 1.00 (100%)
    // When false:
    //   1- Alpha for regular cells are increased back to 1.00
    //   2- Alpha for overlay text is reduced to 0.00
    //
    private var _showMonthNameOverlay: Bool = false
    public func showMonthNameOverlay(_ shouldShow: Bool, animated: Bool) {
        _showMonthNameOverlay = shouldShow

        let otherStuff = [stack, dayOfMonthLabel, monthAbbreviationLabel, yearTextLabel, grayDotImageView, blueCircleImageView]

        var toDo: () -> Void
        if shouldShow {
            toDo = {
                for view in otherStuff {
                    view?.alpha = 0.50
                }
                if self.dayOfMonth >= 12 && self.dayOfMonth <= 18 && self.dayOfWeek == 4 {
                    self.superview?.bringSubview(toFront: self)
                    self.monthNameOverlayLabel?.alpha = 1.00
                } else {
                    self.monthNameOverlayLabel?.alpha = 0.00
                }
            }
        } else {
            toDo = {
                for view in otherStuff {
                    view?.alpha = 1.00
                }
                self.monthNameOverlayLabel?.alpha = 0.00
            }
        }

        DispatchQueue.main.async {
            if animated {
                UIView.animate(withDuration: 0.25, animations: toDo)
            } else {
                toDo()
            }
        }
    }


    //
    // MARK: - UIView References
    //
    private var stack: UIStackView?
    private var dayOfMonthLabel: UILabel?
    private var monthAbbreviationLabel: UILabel?
    private var yearTextLabel: UILabel?
    private var cellUnderlineLayer: CALayer?
    private var grayDotImageView: UIImageView?
    private var blueCircleImageView: UIImageView?
    private var monthNameOverlayLabel: UILabel?


    //
    // MARK: - Cell selection status -- controls blue dot
    //
    override public var isSelected: Bool  {
        didSet {
            updateDisplayForCurrentSettings()
        }
    }

    private var dateTextForegroundColor: UIColor = AppConfig.calendarNormalTextColor {
        didSet {
            monthAbbreviationLabel?.textColor = dateTextForegroundColor
            dayOfMonthLabel?.textColor = dateTextForegroundColor
        }
    }


    // Updates visibility of views based on settings
    private func updateDisplayForCurrentSettings() {
        cellUnderlineLayer?.isHidden = false

        if isSelected {
            dateTextForegroundColor = AppConfig.calendarSelectionTextColor
            blueCircleImageView?.isHidden = false
            monthAbbreviationLabel?.isHidden = true
            yearTextLabel?.isHidden = true
        } else {
            dateTextForegroundColor = AppConfig.calendarNormalTextColor
            blueCircleImageView?.isHidden = true
            if monthAbbreviationLabel != nil && monthAbbreviationLabel!.text != nil && dayOfMonth == 1 {
                monthAbbreviationLabel?.isHidden = false
            } else {
                monthAbbreviationLabel?.isHidden = true
            }
            if yearTextLabel != nil && yearTextLabel!.text != nil && yearText != nil {
                yearTextLabel?.isHidden = false
            } else {
                yearTextLabel?.isHidden = true
            }
        }

        if showGrayDot && dayOfMonth != 1 && !isSelected {
            grayDotImageView?.isHidden = false
        } else {
            grayDotImageView?.isHidden = true
        }

        layoutIfNeeded()
    }

    // MARK: - initCell -- cell customization
    public func initCell(cellData: CalendarCellData, isOverlayModeActive: Bool) {

        // Cell underline
        if cellUnderlineLayer == nil {
            cellUnderlineLayer = CALayer()
            cellUnderlineLayer?.frame = CGRect(x: 0.0, y: frame.height - 1, width: frame.width, height: 1)
            cellUnderlineLayer?.backgroundColor = AppConfig.calendarRowSeparatorColor.cgColor
            layer.addSublayer(cellUnderlineLayer!)
        }

        // Highlighting (blue circle) for selected cell
        if blueCircleImageView == nil {

            blueCircleImageView = UIImageView(frame: frame.insetBy(dx: 6.0, dy: 6.0))
            let diameter = blueCircleImageView!.frame.width
            blueCircleImageView?.image = UIImage.circle(diameter: diameter, color: AppConfig.calendarSelectionColor)
            blueCircleImageView?.contentMode = .scaleAspectFit
            addSubview(blueCircleImageView!)

            blueCircleImageView?.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                NSLayoutConstraint(item: blueCircleImageView!, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.00, constant: 0.00),
                NSLayoutConstraint(item: blueCircleImageView!, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.00, constant: 0.00),
                NSLayoutConstraint(item: blueCircleImageView!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.00, constant: diameter),
                NSLayoutConstraint(item: blueCircleImageView!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.00, constant: diameter),
                ])
        }

        // Stack with main sstuff
        stack?.removeFromSuperview()
        stack = nil

        stack = UIStackView(frame: bounds)
        stack?.axis = .vertical
        stack?.spacing = 0
        stack?.distribution = .fillEqually

        // Month abbreviation
        monthAbbreviationLabel = UILabel()
        monthAbbreviationLabel?.textColor = dateTextForegroundColor
        monthAbbreviationLabel?.textAlignment = .center
        monthAbbreviationLabel?.font = AppConfig.calendarSubtitleFont
        stack?.addArrangedSubview(monthAbbreviationLabel!)

        // Day of month
        dayOfMonthLabel = UILabel()
        dayOfMonthLabel?.textColor = dateTextForegroundColor
        dayOfMonthLabel?.textAlignment = .center
        dayOfMonthLabel?.font = AppConfig.calendarViewFont
        stack?.addArrangedSubview(dayOfMonthLabel!)

        // Year
        yearTextLabel = UILabel()
        yearTextLabel?.textColor = dateTextForegroundColor
        yearTextLabel?.textAlignment = .center
        yearTextLabel?.font = AppConfig.calendarSubtitleFont
        stack?.addArrangedSubview(yearTextLabel!)
        
        addSubview(stack!)

        // Match full size of cell
        stack?.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: stack!, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.00, constant: 0.00),
            NSLayoutConstraint(item: stack!, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.00, constant: 0.00),
            NSLayoutConstraint(item: stack!, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.00, constant: 0.00),
            NSLayoutConstraint(item: stack!, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.00, constant: 0.00)
            ])


        // Gray dot
        if grayDotImageView == nil {
            grayDotImageView = UIImageView(image: UIImage.circle(diameter: 4.0, color: AppConfig.calendarNormalTextColor))
            grayDotImageView?.contentMode = .scaleAspectFit
            addSubview(grayDotImageView!)

            grayDotImageView?.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                NSLayoutConstraint(item: grayDotImageView!, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.00, constant: 0.00),
                NSLayoutConstraint(item: grayDotImageView!, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.00, constant: -4.00),
                NSLayoutConstraint(item: grayDotImageView!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.00, constant: 4.00),
                NSLayoutConstraint(item: grayDotImageView!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.00, constant: 4.00),
                ])
        }

        // Month Name Overlay
        if monthNameOverlayLabel == nil {
            monthNameOverlayLabel = UILabel()
            monthNameOverlayLabel?.textColor = AppConfig.calendarMonthOverlayColor
            monthNameOverlayLabel?.textAlignment = .center
            monthNameOverlayLabel?.font = AppConfig.calendarViewFont
            monthNameOverlayLabel?.isHidden = false
            monthNameOverlayLabel?.alpha = 0.00
            monthNameOverlayLabel?.clipsToBounds = false

            addSubview(monthNameOverlayLabel!)

            monthNameOverlayLabel?.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                NSLayoutConstraint(item: monthNameOverlayLabel!, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.00, constant: 0.00),
                NSLayoutConstraint(item: monthNameOverlayLabel!, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.00, constant: 0.00),
                NSLayoutConstraint(item: monthNameOverlayLabel!, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 6.00, constant: 0.00)
            ])
        }

        // Store the properties we were given
        self.dayOfMonth = cellData.dayOfMonth
        self.monthAbbreviation = cellData.monthAbbreviation
        self.yearText = cellData.yearText
        self.showGrayDot = cellData.showGrayDot
        self.isCellBackgroundShaded = cellData.isCellBackgroundShaded
        monthNameOverlayText = cellData.monthName
        showMonthNameOverlay(isOverlayModeActive, animated: false)
        self.dayOfWeek = cellData.dayOfWeek

        // Be sure this cell is in front so the month overlay can span multiple cells
        if self.dayOfMonth >= 12 && self.dayOfMonth <= 18 && self.dayOfWeek == 4 {
            clipsToBounds = false
        }
        // Will show/hide as needed
        updateDisplayForCurrentSettings()
    }


    // MARK: - LIFECYCLE
    private func customInit() {
        layer.backgroundColor = UIColor.clear.cgColor
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }
}
