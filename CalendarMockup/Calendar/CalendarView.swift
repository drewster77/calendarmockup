//
//  CalendarView.swift
//  CalendarMockup
//
//  Created on 8/31/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import Foundation
import UIKit

protocol CalendarViewDelegate {
    func calendarDidSelectDate(_ date: Date)
    func calendarScrollingWillBegin()
    func calendarScrollingDidEnd()
}

class CalendarView: UIView {
    // MARK: - STATIC
    private static let headerHeight: CGFloat = 38.0
    public static func getPreferredViewHeightForViewWithWidth(_ width: CGFloat, usingCompactMode compactMode: Bool) -> CGFloat {
        let weeksToDisplay: Double = compactMode ? 2 : 5
        return getCellHeightForViewWithWidth(width) * CGFloat(weeksToDisplay + 0.5) + CalendarView.headerHeight
    }

    public static func getCellWidthForViewWithWidth(_ width: CGFloat) -> CGFloat {
        return width / 7.0
    }

    public static func getCellHeightForViewWithWidth(_ width: CGFloat) -> CGFloat {
        return getCellWidthForViewWithWidth(width)
    }

    public static func itemSizeForViewWithWidth(_ width: CGFloat) -> CGSize {
        return CGSize(width: getCellWidthForViewWithWidth(width), height: getCellHeightForViewWithWidth(width))
    }

    // MARK: - PUBLIC
    public var calendarViewDelegate: CalendarViewDelegate?
    public var cellData = [CalendarCellData]() {
        didSet {
            collectionView?.cellData = cellData
            reloadData()
        }
    }

    public func getHeightForMode(usingCompactMode compactMode: Bool) -> CGFloat {
        return CalendarView.getPreferredViewHeightForViewWithWidth(frame.width, usingCompactMode: compactMode)
    }

    public func selectItemWithDate(_ date: Date, animated: Bool) {
        collectionView?.scrollToDate(date, animated: animated)
        updateMonthTitleForDate(date)
        
    }

    public func reloadData() {
        collectionView?.reloadData()

        collectionView?.scrollToDate(Date(), animated: true)
        updateMonthTitleForCurrentSelection()
    }

    
    // MARK: - PRIVATE PROPERTIES
    private var hamburgerImageView = UIImageView(image: UIImage(named: "hamburgerButton"))
    private var monthNameLabel: UILabel?
    private var viewTypeSelectorImageView = UIImageView(image: UIImage(named: "viewTypeSelectorButton"))
    private var plusButtonImageView = UIImageView(image: UIImage(named: "plusButton"))
    private var titleLabels = [UILabel]()
    private var collectionView: CalendarCollectionView?

    private func updateMonthTitleForDate(_ date: Date) {
        let monthNumber = date.monthOfYear()
        let month = MonthOfTheYear(rawValue: monthNumber)!.name
        DispatchQueue.main.async {
            self.monthNameLabel?.text = month
        }

    }
    private func updateMonthTitleForCurrentSelection() {
        guard let collectionView = collectionView else {return}

        for path in collectionView.indexPathsForSelectedItems ?? [IndexPath]() {
            let date = collectionView.dateForIndexPath(path)
            updateMonthTitleForDate(date)
        }
    }


    // Faux buttons at the top
    //
    // In a complete app, these buttons would link to other functions, such as
    // adding new events, editing, etc.
    private func setupHeaderButtons() {

        guard titleLabels.count == 7 else {
            fatalError("Labels must be created before header buttons")
        }
        let width = CalendarView.getCellWidthForViewWithWidth(frame.width)
        let height = CalendarView.headerHeight

        var constraints = [NSLayoutConstraint]()

        // Align buttons to the top, same size as cells
        for imageView in [hamburgerImageView, viewTypeSelectorImageView, plusButtonImageView] {
            imageView.contentMode = .scaleAspectFill
            imageView.layer.borderWidth = 0.0
            addSubview(imageView)

            imageView.translatesAutoresizingMaskIntoConstraints = false
            constraints += [
                NSLayoutConstraint(item: imageView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.00, constant: 0.00),
                NSLayoutConstraint(item: imageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.00, constant: width / 2.0),
                NSLayoutConstraint(item: imageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.00, constant: height / 2.0)
            ]
        }

        // Align with Sunday, Friday, and Saturday cells
        constraints += [
            NSLayoutConstraint(item: hamburgerImageView, attribute: .centerX, relatedBy: .equal, toItem: titleLabels[0], attribute: .centerX, multiplier: 1.00, constant: 0.00),
            NSLayoutConstraint(item: viewTypeSelectorImageView, attribute: .centerX, relatedBy: .equal, toItem: titleLabels[5], attribute: .centerX, multiplier: 1.00, constant: 0.00),
            NSLayoutConstraint(item: plusButtonImageView, attribute: .centerX, relatedBy: .equal, toItem: titleLabels[6], attribute: .centerX, multiplier: 1.00, constant: 0.00)
        ]


        // Header label
        monthNameLabel = UILabel(frame: bounds)
        monthNameLabel?.font = AppConfig.monthTitleLabelFont
        monthNameLabel?.textColor = AppConfig.calendarSelectionColor
        monthNameLabel?.textAlignment = .center
        addSubview(monthNameLabel!)

        // Constraints
        monthNameLabel?.translatesAutoresizingMaskIntoConstraints = false
        constraints += [
            NSLayoutConstraint(item: monthNameLabel!, attribute: .centerY, relatedBy: .equal, toItem: plusButtonImageView, attribute: .centerY, multiplier: 1.00, constant: 0.00),
            NSLayoutConstraint(item: monthNameLabel!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.00, constant: width * 4.0),
            NSLayoutConstraint(item: monthNameLabel!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.00, constant: height),
            NSLayoutConstraint(item: monthNameLabel!, attribute: .centerX, relatedBy: .equal, toItem: titleLabels[3], attribute: .centerX, multiplier: 1.00, constant: 0.00)
        ]

        NSLayoutConstraint.activate(constraints)
        layoutIfNeeded()

    }

    // Titles (days of week)
    private func setupTitleLabels() {

        let width = CalendarView.getCellWidthForViewWithWidth(frame.width)
        let height = CalendarView.getCellHeightForViewWithWidth(frame.width) * 0.50

        var constraints = [NSLayoutConstraint]()

        var viewToLeft: UIView!
        for dow in 0 ..< 7 {

            let label = UILabel()
            label.textAlignment = .center
            label.text = DayOfWeek(rawValue: dow)!.abbreviation
            label.font = AppConfig.calendarTitleFont
            label.textColor = AppConfig.calendarNormalTextColor
            label.backgroundColor = UIColor.clear

            titleLabels.append(label)
            addSubview(label)

            // Cell underline
            let underline = CALayer()
            underline.frame = CGRect(x: 0.0, y: label.frame.height - 1, width: label.frame.width, height: 1)
            underline.backgroundColor = AppConfig.calendarRowSeparatorColor.cgColor
            label.layer.addSublayer(underline)


            // Constraints
            label.translatesAutoresizingMaskIntoConstraints = false
            if dow == 0 {
                constraints += [NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.00, constant: 0.00)]
            } else {
                constraints += [NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: viewToLeft, attribute: .right, multiplier: 1.00, constant: 0.00)]
            }
            viewToLeft = label

            constraints += [
                NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.00, constant: CalendarView.headerHeight),
                NSLayoutConstraint(item: label, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.00, constant: width),
                NSLayoutConstraint(item: label, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.00, constant: height)
            ]
        }

        NSLayoutConstraint.activate(constraints)
        layoutIfNeeded()
    }



    // Collection View
    private func setupCollectionView() {
        collectionView = CalendarCollectionView(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height), collectionViewLayout: CalendarFlowLayout(itemSize: CalendarView.itemSizeForViewWithWidth(frame.width)))
        collectionView?.calendarDelegate = self
        collectionView?.register(CalendarCollectionViewCell.self, forCellWithReuseIdentifier: "CalendarCollectionViewCell")
        collectionView?.backgroundColor = AppConfig.calendarNormalBackgroundColor
        addSubview(collectionView!)

        // Fit to remaining space
        collectionView?.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: collectionView!, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.00, constant: 0.00),
            NSLayoutConstraint(item: collectionView!, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.00, constant: 0.00),
            NSLayoutConstraint(item: collectionView!, attribute: .top, relatedBy: .equal, toItem: titleLabels[0], attribute: .bottom, multiplier: 1.00, constant: 0.00),
            NSLayoutConstraint(item: collectionView!, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.00, constant: 0.00)
            ])

        layoutIfNeeded()
    }

    // View setup - run once
    private func setupSubviews() {
        if titleLabels.count == 0 {
            setupTitleLabels()
            setupHeaderButtons()
        }

        if collectionView == nil {
            setupCollectionView()
        }
    }


    // MARK: - LIFECYCLE
    private func customInit() {
        layer.borderWidth = 0
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        setupSubviews()
    }
}



extension CalendarView: CalendarDelegate {
    func calendarScrollingWillBegin() {
        calendarViewDelegate?.calendarScrollingWillBegin()
    }

    func calendarScrollingDidEnd() {
        calendarViewDelegate?.calendarScrollingDidEnd()
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath, withDate date: Date) {
        updateMonthTitleForCurrentSelection()
        calendarViewDelegate?.calendarDidSelectDate(date)
    }
}
