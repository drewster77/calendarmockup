//
//  CalenderFlowLayout.swift
//  CalendarMockup
//
//  Created on 9/1/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import Foundation
import UIKit

class CalendarFlowLayout: UICollectionViewFlowLayout {

    convenience init(itemSize: CGSize) {
        self.init()
        self.itemSize = itemSize
    }
    
    private var setupDone = false
    override func prepare() {
        super.prepare()
        if !setupDone {
            setup()
            setupDone = true
        }
    }

    private func setup() {
        scrollDirection = .vertical
        minimumLineSpacing = 0
        minimumInteritemSpacing = 0
    }

    // This thing makes sure we snap to a row, and don't stop scrolling in between rows
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        guard let collectionView = collectionView else {
            return super.targetContentOffset(forProposedContentOffset: proposedContentOffset, withScrollingVelocity: velocity)

        }

        var offsetAdjustment = CGFloat.greatestFiniteMagnitude
        let verticalOffset = proposedContentOffset.y + collectionView.contentInset.top
        let targetRect = CGRect(x: 0, y: proposedContentOffset.y, width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)

        let layoutAttributesArray = super.layoutAttributesForElements(in: targetRect)

        layoutAttributesArray?.forEach({ (layoutAttributes) in
            let itemOffset = layoutAttributes.frame.origin.y
            if fabsf(Float(itemOffset - verticalOffset)) < fabsf(Float(offsetAdjustment)) {
                offsetAdjustment = itemOffset - verticalOffset
            }
        })

        return CGPoint(x: proposedContentOffset.x, y: proposedContentOffset.y + offsetAdjustment)
    }
}
