//
//  CalendarViewController.swift
//  CalendarMockup
//
//  Created on 8/31/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import Foundation
import UIKit

class CalendarViewController: UIViewController {

    var calendarView: CalendarView?
    var agendaView: AgendaView?
    var calendarShouldUseCompactMode: Bool = false {
        didSet {
            // Animate update to frame sizes as needed
            DispatchQueue.main.async {
                let newHeight = self.calendarView!.getHeightForMode(usingCompactMode: self.calendarShouldUseCompactMode)
                UIView.animate(withDuration: 0.25, animations: {
                    self.calendarHeightConstraint?.constant = newHeight
                    self.view.layoutIfNeeded()
                }, completion: { (completed) in
                    print("Animation completed: \(completed)")
                })
            }
        }
    }
    var calendarHeightConstraint: NSLayoutConstraint?


    var calendarData = [CalendarEvent]() {
        // Create data models for both calendarView and agendaView when our calendar data is updated
        didSet {
            var calData = [CalendarCellData]()
            var agendaData = [AgendaCellData]()

            for event in calendarData {

                // Add calendar model item
                let dom = event.startTime.dayOfMonth()
                let moy = MonthOfTheYear(rawValue: event.startTime.monthOfYear())!
                let monthAbbrev = dom == 1 ? moy.abbreviation : nil
                let yearText = dom == 1 && moy.rawValue == 1 ? "\(event.startTime.dateComponents().year!)" : nil

                let calItem = CalendarCellData(startTime: event.startTime, dayOfMonth: event.startTime.dayOfMonth(), monthAbbreviation: monthAbbrev, yearText: yearText, showGrayDot: true, isCellBackgroundShaded: false, monthName: moy.name, dayOfWeek: event.startTime.dayOfWeek())
                calData.append(calItem)

                // Add agenda model item
                let images = event.attendees.map { (attendee) -> UIImage in
                    return attendee.image
                }

                let agendaItem = AgendaCellData(referenceID: UUID(), startTime: event.startTime, length: event.length, dotIcon: event.category.icon, title: event.title, images: images, location: event.location)
                agendaData.append(agendaItem)
            }

            calendarView?.cellData = calData
            agendaView?.cellData = agendaData
            
        }
    }
    // MARK: - SUBVIEW SETUP AND SIZING
    private func initFramesAndConstraints(view: UIView, cal: CalendarView, agenda: AgendaView) {

        // Initial calendar frame height
        let initialCalHeight = CalendarView.getPreferredViewHeightForViewWithWidth(view.frame.width, usingCompactMode: false)

        // Constraints to enforce what we want
        cal.translatesAutoresizingMaskIntoConstraints = false
        agenda.translatesAutoresizingMaskIntoConstraints = false
        calendarHeightConstraint = NSLayoutConstraint(item: cal, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.00, constant: initialCalHeight)
        NSLayoutConstraint.activate([
            // Calendar frame
            NSLayoutConstraint(item: cal, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1.00, constant: 0.00),
            NSLayoutConstraint(item: cal, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1.00, constant: 0.00),
            NSLayoutConstraint(item: cal, attribute: .top, relatedBy: .equal, toItem: view.safeAreaLayoutGuide, attribute: .top, multiplier: 1.00, constant: 0.00),
            calendarHeightConstraint!,

            // Agenda frame
            NSLayoutConstraint(item: agenda, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1.00, constant: 0.00),
            NSLayoutConstraint(item: agenda, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1.00, constant: 0.00),
            NSLayoutConstraint(item: agenda, attribute: .top, relatedBy: .equal, toItem: cal, attribute: .bottom, multiplier: 1.00, constant: 0.00),
            NSLayoutConstraint(item: agenda, attribute: .bottom, relatedBy: .equal, toItem: view.safeAreaLayoutGuide, attribute: .bottom, multiplier: 1.00, constant: 0.00),
            ])

        view.layoutIfNeeded()
    }

    // MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()

        calendarView = CalendarView()
        calendarView?.calendarViewDelegate = self

        agendaView = AgendaView()
        agendaView?.agendaViewDelegate = self

        view.addSubview(calendarView!)
        view.addSubview(agendaView!)

        initFramesAndConstraints(view: view, cal: calendarView!, agenda: agendaView!)

        // Load dummy data
        calendarData = DummyData().events
        calendarView?.reloadData()
        agendaView?.reloadData()

        // Select today's date
        calendarView?.selectItemWithDate(Date(), animated: false)
        agendaView?.selectItemWithDate(Date())
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // Load dummy data -- in reality this would be a real data refresh, but would
        // ideally check for changes, and not just blindly reload, as we are doing now.
        calendarData = DummyData().events
        calendarView?.reloadData()
        agendaView?.reloadData()
    }
}


// MARK: - CalendarViewDelegate -- Various callbacks from the CalendarView
extension CalendarViewController: CalendarViewDelegate {

    // Expand calendar if it's being scrolled
    func calendarScrollingWillBegin() {
        if calendarShouldUseCompactMode {
            calendarShouldUseCompactMode = false
        }
    }

    func calendarScrollingDidEnd() {

    }

    // Change agenda selection if calendar has a date selected
    func calendarDidSelectDate(_ date: Date) {
        agendaView?.selectItemWithDate(date)
    }
}


// MARK: - AgendaViewDelegate -- Various callbacks from AgendaView
extension CalendarViewController: AgendaViewDelegate {
    func agendaScrollingWillBegin() {
        if !calendarShouldUseCompactMode {
            calendarShouldUseCompactMode = true
        }
    }

    func agendaScrollingDidEnd() {

    }

    // Update calendar selection if agenda is scrolled
    func agendaDidSelectDate(_ date: Date, viaScrolling: Bool) {
        if viaScrolling {
            calendarView?.selectItemWithDate(date, animated: false)
        }
    }
}
