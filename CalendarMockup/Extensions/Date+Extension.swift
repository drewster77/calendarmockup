//
//  Date+Extension.swift
//  CalendarMockup
//
//  Created on 9/1/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import Foundation

extension Date {

    // MARK: - Handy initializers
    init(year: Int, month: Int, day: Int, hr: Int, min: Int, sec: Int)  {
        let calendar = Calendar(identifier: .gregorian)
        let components = DateComponents(year: year, month: month, day: day, hour: hr, minute: min, second: sec)
        self = calendar.date(from: components)!
    }

    init(year: Int, month: Int, day: Int)  {
        self.init(year: year, month: month, day: day, hr: 0, min: 0, sec: 0)
    }

    func offsetBy(days:Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: days, to: self)!
    }

    func endOfDay() -> Date {
        let comp = self.dateComponents()
        return Date(year: comp.year!, month: comp.month!, day: comp.day!, hr: 23, min: 59, sec: 59)
    }

    // MARK: - Comparisons
    func occursOnSameDayAs(_ other: Date) -> Bool {
        let comp1 = self.dateComponents()
        let comp2 = other.dateComponents()

        return comp1.year == comp2.year && comp1.month == comp2.month && comp1.day == comp2.day
    }


    // MARK: - Break out individual components
    func dateComponents() -> DateComponents {
        let comp = Calendar.autoupdatingCurrent.dateComponents([.year, .month, .day, .hour, .minute, .second, .weekday], from: self)
        return comp
    }

    func dayOfMonth() -> Int {
        let comp = dateComponents()
        if let day = comp.day {
            return day
        } else {
            fatalError("bogus day of month for date \(self)")
        }
    }

    func dayOfWeek() -> Int {
        let comp = dateComponents()
        if let weekday = comp.weekday {
            return weekday
        } else {
            fatalError("bogus day of week for date \(self)")
        }
    }

    func monthOfYear() -> Int {
        let comp = dateComponents()
        if let month = comp.month {
            return month
        } else {
            fatalError("bogus month for date \(self)")
        }
    }


    // MARK: - Formatted strings
    var timeAsString: String {
        let fmt = DateFormatter()
        fmt.dateStyle = .none
        fmt.timeStyle = .short

        return fmt.string(from: self)
    }

    var dateAsStringFull: String {
        let fmt = DateFormatter()
        fmt.dateStyle = .full
        fmt.timeStyle = .none

        return fmt.string(from: self)
    }
}
