//
//  TimeInterval+Extension.swift
//  CalendarMockup
//
//  Created on 9/2/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import Foundation


extension TimeInterval {

    // MARK: - Conversions
    var wholeHours: Int {
        return Int(self) / (60*60)
    }

    var wholeMinutes: Int {
        return Int(self) / 60
    }

    // MARK: - Formatted string
    var asString: String {

        let hours = wholeHours
        let remaining: TimeInterval = self - Double(wholeHours * 3600)
        let minutes = remaining.wholeMinutes

        var result = ""
        if hours > 0 {
            result += "\(hours)h "
        }
        if minutes > 0 {
            result += "\(minutes)m"
        }

        return result.trimmingCharacters(in: CharacterSet([" "]))
    }
}
