//
//  UICollectionViewCell+Extension.swift
//  CalendarMockup
//
//  Created on 9/7/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionViewCell {
    func applyContentViewConstraints() {

        // remove any existing constraints between self and contentView
        for constraint in constraints {
            if constraint.isActive && (
                (constraint.firstItem === self && constraint.secondItem === contentView) ||
                    (constraint.firstItem === contentView && constraint.secondItem === self)) {
                removeConstraint(constraint)
            }
        }

        // Add new ones
        contentView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: contentView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.00, constant: 0.00),
            NSLayoutConstraint(item: contentView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.00, constant: 0.00),
            NSLayoutConstraint(item: contentView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.00, constant: 0.00),
            NSLayoutConstraint(item: contentView, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.00, constant: 0.00),
            ])
    }
}
