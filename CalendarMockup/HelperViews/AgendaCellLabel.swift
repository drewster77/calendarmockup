//
//  AgendaCellLabel.swift
//  CalendarMockup
//
//  Created on 9/7/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import Foundation
import UIKit


// Subclassed to set intrinsicContentSize, which makes autolayout happier
class AgendaCellLabel: UILabel {
    private var intrinsicSizeToUse: CGSize?
    override var intrinsicContentSize: CGSize {
        return intrinsicSizeToUse ?? super.intrinsicContentSize
    }


    convenience init(size: CGSize) {
        self.init()

        intrinsicSizeToUse = size
        invalidateIntrinsicContentSize()
    }

}
