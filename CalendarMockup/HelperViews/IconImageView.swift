//
//  IconImageView.swift
//  CalendarMockup
//
//  Created on 9/6/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import Foundation
import UIKit

// Subclassed to set intrinsicContentSize and to control making the
// image round and/or border colors
class IconImageView: UIImageView {

    static let iconImageSize = CGSize(width: 32, height: 32)
    
    private var intrinsicSizeToUse: CGSize?
    override var intrinsicContentSize: CGSize {
        return intrinsicSizeToUse ?? IconImageView.iconImageSize
    }


    convenience init(size: CGSize, rounded: Bool, borderColor: UIColor) {
        self.init()

        intrinsicSizeToUse = size
        invalidateIntrinsicContentSize()

        if rounded {
            layer.cornerRadius = IconImageView.iconImageSize.width / 2
            clipsToBounds = true
        }
        layer.borderWidth = 1.0
        layer.borderColor = borderColor.cgColor
    }
    
    convenience init(borderColor: UIColor) {
        self.init()

        layer.cornerRadius = IconImageView.iconImageSize.width / 2
        clipsToBounds = true
        layer.borderWidth = 1.0
        layer.borderColor = borderColor.cgColor
    }
}
