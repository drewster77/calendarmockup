//
//  AgendaCellData.swift
//  CalendarMockup
//
//  Created on 9/2/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import Foundation
import UIKit


// AgendaCellData
//
// The data model used by AgendaCollectionViewCell.
// In a complete app, the referenceID field would probably refer to
// a UUID or other reference to the source of truth for event data.
//
struct AgendaCellData {
    public let referenceID: UUID

    public let startTime: Date
    public let length: TimeInterval?

    public let dotIcon: UIImage

    public let title: String
    public let images: [UIImage]
    public let location: String?

    public var lengthAsString: String {
        guard let length = length else {
            return "ALL DAY"
        }

        return length.asString
    }
}
