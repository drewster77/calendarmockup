//
//  CalendarCellData.swift
//  CalendarMockup
//
//  Created on 9/2/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import Foundation


// CalendarCellData
//
// The data model a calendar cell needs.
// In a complete app, this wouldn't really be much different.
// dayOfMonth, monnthAbbreviation, yearText, monthName, and
// dayOfWeek can all be derived from startTime, so this logic
// could be moved into the AgendaCollectionViewCell, rather than
// being determined by the AgendaCollectionView itself.
//
struct CalendarCellData {
    let startTime: Date
    let dayOfMonth: Int
    let monthAbbreviation: String?
    let yearText: String?
    let showGrayDot: Bool
    let isCellBackgroundShaded: Bool
    let monthName: String?
    let dayOfWeek: Int
}
