//
//  CalendarEventswift
//  CalendarMockup
//
//  Created on 9/2/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import Foundation
import UIKit


// CalendarEvent
//
// CalendarEvent is the main data class the calendar is using.
// In a complete app, the events would also contain additional items, such
// as notification preferences.
//
class CalendarEvent {

    // MARK: - Data fields
    public let id: UUID

    public var title: String
    public var description: String?
    public var location: String?

    public var startTime: Date
    public var length: TimeInterval?

    public var attendees: [CalendarEventAttendee]
    public var category: CalendarEventCategory

    public var isAllDayEvent: Bool {
        return length == nil
    }
    public var endTime: Date? {
        guard !isAllDayEvent else {
            return nil
        }

        if length != nil {
            return startTime.addingTimeInterval(length!)
        } else {
            return startTime.endOfDay()
        }
    }


    // MARK: - Initializers
    init(id: UUID, title: String, description: String?, location: String?, startTime: Date, length: TimeInterval?, attendees: [CalendarEventAttendee], category: CalendarEventCategory) {
        self.id = id
        self.title = title
        self.description = description
        self.location = location
        self.startTime = startTime
        self.length = length
        self.attendees = attendees
        self.category = category
    }

    convenience init(id: UUID, title: String, description: String?, location: String?, startTime: Date, length: TimeInterval?, attendees: [CalendarEventAttendee]) {

        self.init(id: id, title: title, description: description, location: location, startTime: startTime, length: length, attendees: attendees, category: .other)
    }

    convenience init(title: String, description: String?, location: String?, startTime: Date, length: TimeInterval?, attendees: [CalendarEventAttendee]) {
        self.init(id: UUID(), title: title, description: description, location: location, startTime: startTime, length: length, attendees: attendees, category: .other)
    }

    convenience init(title: String, startTime: Date) {
        self.init(id: UUID(), title: title, description: nil, location: nil, startTime: startTime, length: nil, attendees: [CalendarEventAttendee](), category: .other)
    }
}
