//
//  CalendarEventAttendee.swift
//  CalendarMockup
//
//  Created on 9/2/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import Foundation
import UIKit


// CalendarEventAttendee
//
// This struct is a reference to an attendee for an event.
// In a complete app, this might be a reference to a full contact
// entry for the individual.  Also, the image would probably be
// either a URL or maybe a reference to an image cache.
//
struct CalendarEventAttendee  {
    let id: UUID

    var firstName: String
    var lastName: String
    var image: UIImage

    var fullName: String {
        return "\(firstName) \(lastName)"
    }

}
