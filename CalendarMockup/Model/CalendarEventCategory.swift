//
//  CalendarEventCategory.swift
//  CalendarMockup
//
//  Created on 9/6/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import Foundation
import UIKit

// CalendarEventCategory
//
// Categories of possible events.
// In a complete app, this would be a bigger list and would probably
// be set up in a way that it's user-extensible.
//
enum CalendarEventCategory {
    case meeting
    case party
    case phoneCall
    case social
    case other

    var icon: UIImage {
        switch self {
        case .meeting:  return UIImage.circle(diameter: 10.0, color: UIColor.red)
        case .party:    return UIImage.circle(diameter: 10.0, color: UIColor.green)
        case .phoneCall: return UIImage.circle(diameter: 10.0, color: UIColor.blue)
        case .social:   return UIImage.circle(diameter: 10.0, color: UIColor.yellow)
        case .other:    return UIImage.circle(diameter: 10.0, color: UIColor.brown)
        }
    }
}
