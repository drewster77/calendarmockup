//
//  DayOfWeek.swift
//  CalendarMockup
//
//  Created on 9/2/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import Foundation


public enum DayOfWeek: Int {
    case sunday = 0, monday, tuesday, wednesday, thursday, friday, saturday

    var abbreviation: String {
        switch self {
        case .sunday:   return "S"
        case .monday:   return "M"
        case .tuesday:  return "T"
        case .wednesday: return "W"
        case .thursday: return "T"
        case .friday:   return "F"
        case .saturday: return "S"
        }
    }
}
