//
//  DummyData.swift
//  CalendarMockup
//
//  Created by 9/6/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import Foundation
import UIKit

// DummyData
//
// Exactly what it sounds like. 😀
//
// In a complete app, this would reference real data somewhere, probably in the cloud,
// with a local cache, such as an iCloud/CoreData combo, Realm, Firebase, etc..
class DummyData {
    let person1 = CalendarEventAttendee(id: UUID(), firstName: "Hank", lastName: "Hill", image: UIImage(named: "face1")!)
    let person2 = CalendarEventAttendee(id: UUID(), firstName: "Gioachino", lastName: "Rossini", image: UIImage(named: "face2")!)
    let person3 = CalendarEventAttendee(id: UUID(), firstName: "Hank", lastName: "Hill", image: UIImage(named: "face3")!)
    let person4 = CalendarEventAttendee(id: UUID(), firstName: "Hank", lastName: "Hill", image: UIImage(named: "face4")!)

    var events: [CalendarEvent] {
        get {
            return [

                CalendarEvent(title: "Burger with Joe", startTime: Date.init(year: 2018, month: 09, day: 17, hr: 12, min: 15, sec: 0)),
                CalendarEvent(title: "Coffee with Samuel Jackson", startTime: Date.init(year: 2018, month: 09, day: 20, hr: 08, min: 15, sec: 0)),
                CalendarEvent(title: "Zac's soccer", startTime: Date.init(year: 2018, month: 09, day: 15, hr: 2, min: 0, sec: 0)),
                CalendarEvent(title: "Accountant coming here", startTime: Date.init(year: 2018, month: 10, day: 3, hr: 4, min: 0, sec: 0)),

                CalendarEvent(title: "Hair cut", description: nil, location: "Seville", startTime: Date.init(year: 2018, month: 10, day: 4, hr: 18, min: 10, sec: 0), length: 45*60, attendees: [person2]),
                CalendarEvent(title: "Leave early", description: nil, location: nil, startTime: Date.init(year: 2018, month: 9, day: 21, hr: 14, min: 45, sec: 0), length: 120*60, attendees: [person1, person3]),
                CalendarEvent(title: "Marketing meeting", description: nil, location: "127 Garrison Rd S, Bakersfield", startTime: Date.init(year: 2018, month: 10, day: 1, hr: 7, min: 30, sec: 0), length: 150*60, attendees: [person1, person3, person4, person2]),
                CalendarEvent(title: "Dinner w/ Falcones", description: nil, location: "Flying Saucer, 14712 NE 91st St, Redmond, WA 98052", startTime: Date.init(year: 2018, month: 9, day: 26, hr: 19, min: 0, sec: 0), length: 90*60, attendees: [person1, person4]),
                CalendarEvent(title: "Vacuum the living, basement, and kids rooms before Claire and Grace arrive from Tulsa", description: nil, location: nil, startTime: Date.init(year: 2018, month: 9, day: 26, hr: 15, min: 30, sec: 0), length: nil, attendees: [CalendarEventAttendee]())
            ]
        }
    }

}
