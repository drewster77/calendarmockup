//
//  AgendaCollectionViewCellTests.swift
//  CalendarMockupTests
//
//  Created on 9/8/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import XCTest
@testable import CalendarMockup

class AgendaCollectionViewCellTests: XCTestCase {
    
    var cell: AgendaCollectionViewCell!

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.

        cell = AgendaCollectionViewCell(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }


    func testInitCell() {
        let id = UUID()
        let date = Date()
        let length = TimeInterval(90 * 60)
        let icon = UIImage.circle(diameter: 32, color: UIColor.orange)
        let data = AgendaCellData(referenceID: id, startTime: date, length: length, dotIcon: icon, title: "Lunch with Larry", images: [UIImage](), location: nil)
        cell.initCell(cellData: data)

    }
    
}
