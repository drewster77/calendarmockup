//
//  AgendaCollectionViewTests.swift
//  CalendarMockupTests
//
//  Created on 9/8/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import XCTest
@testable import CalendarMockup

class AgendaCollectionViewTests: XCTestCase {
    
    var cv: AgendaCollectionView!

    override func setUp() {
        super.setUp()

        cv = AgendaCollectionView(frame: CGRect(x: 0, y: 0, width: 320, height: 120), collectionViewLayout: UICollectionViewFlowLayout())
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }


    func testDateIndexPathConversions() {
        // Be sure 2-way conversion works for a handful of dates

        let dates: [Date] = [
            AppConfig.dateEpoch,
            Date(year: 2017, month: 12, day: 31),
            Date(year: 2018, month: 04, day: 19),
            Date(year: 2019, month: 01, day: 01),
            Date(year: 2035, month: 01, day: 31)
        ]

        for date in dates {
            let indexPath = cv.indexPathForDate(date)
            XCTAssertGreaterThanOrEqual(indexPath.row, 0, "Row for \(date) must be >= 0")
            XCTAssertGreaterThanOrEqual(indexPath.section, 0, "Section for \(date) must be >= 0")

            let newDate = cv.dateForIndexPath(indexPath)
            XCTAssertGreaterThanOrEqual(newDate, AppConfig.dateEpoch, "Date cannot be before the epoch \(AppConfig.dateEpoch)")
            XCTAssertEqual(date, newDate, "date '\(date)' must equal '\(newDate)'")
        }
    }
    
}
