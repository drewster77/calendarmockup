//
//  AgendaNoEventsCollectionViewCellTests.swift
//  CalendarMockupTests
//
//  Created on 9/8/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import XCTest
@testable import CalendarMockup

class AgendaNoEventsCollectionViewCellTests: XCTestCase {

    var cell: AgendaNoEventsCollectionViewCell!

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.

        cell = AgendaNoEventsCollectionViewCell(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }


    func testInitCell() {
        cell.initCell()
    }
}
