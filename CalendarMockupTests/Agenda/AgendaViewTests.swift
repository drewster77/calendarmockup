//
//  AgendaViewTests.swift
//  CalendarMockupTests
//
//  Created on 9/8/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import XCTest
@testable import CalendarMockup


class AgendaViewTests: XCTestCase {

    var agenda: AgendaView!

    override func setUp() {
        super.setUp()
        agenda = AgendaView(frame: CGRect(x: 0, y: 200, width: 320, height: 500))
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }    
}
