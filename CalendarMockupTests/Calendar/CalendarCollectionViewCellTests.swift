//
//  CalendarCollectionViewCellTests.swift
//  CalendarMockupTests
//
//  Created on 9/8/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import XCTest
@testable import CalendarMockup

class CalendarCollectionViewCellTests: XCTestCase {

    var cell: CalendarCollectionViewCell!

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.

        cell = CalendarCollectionViewCell(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }


    func testInitCell() {
        let data = CalendarCellData(startTime: Date(year: 2021, month: 02, day: 28, hr: 20, min: 30, sec: 0), dayOfMonth: 28, monthAbbreviation: "Feb", yearText: "2021", showGrayDot: true, isCellBackgroundShaded: false, monthName: "February", dayOfWeek: 0)
        cell.initCell(cellData: data, isOverlayModeActive: false)

        XCTAssertEqual(cell.dayOfMonth, 28)
        XCTAssertEqual(cell.monthAbbreviation, "Feb")
        XCTAssertEqual(cell.yearText, "2021")
        XCTAssertEqual(cell.showGrayDot, true)
        XCTAssertEqual(cell.dayOfWeek, 0)
        XCTAssertEqual(cell.isCellBackgroundShaded, false)
        XCTAssertEqual(cell.monthNameOverlayText, "February")
    }


    
}
