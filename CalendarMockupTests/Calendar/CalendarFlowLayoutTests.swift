//
//  CalendarFlowLayoutTests.swift
//  CalendarMockupTests
//
//  Created on 9/8/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import XCTest
@testable import CalendarMockup


class CalendarFlowLayoutTests: XCTestCase {

    var cv: CalendarCollectionView!
    var layout: CalendarFlowLayout!

    override func setUp() {
        super.setUp()
        layout = CalendarFlowLayout(itemSize: CGSize(width: 40, height: 40))
        cv = CalendarCollectionView(frame: CGRect(x: 0, y: 0, width: 320, height: 120),
                                    collectionViewLayout: layout)

    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testTargetContentOffset() {
        // Target offset should be the same in the 1st half of the itemSize
        let offsetAtZero = layout.targetContentOffset(forProposedContentOffset: CGPoint(x: 0, y: 0), withScrollingVelocity: CGPoint(x: 0, y: 500))
        for yOffset in 1 ..< 20 {
            let offset = layout.targetContentOffset(forProposedContentOffset: CGPoint(x: 0, y: yOffset), withScrollingVelocity: CGPoint(x: 0, y: 500))
            XCTAssertEqual(offsetAtZero, offset)
        }
    }
    
}
