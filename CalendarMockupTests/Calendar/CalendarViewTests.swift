//
//  CalendarViewTests.swift
//  CalendarMockupTests
//
//  Created on 9/8/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import XCTest
@testable import CalendarMockup

class CalendarViewTests: XCTestCase {

    private var cv: CalendarView!

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        cv = CalendarView(frame: CGRect(x: 0, y: 0, width: 300, height: 200))
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    

    func testStaticMethods() {
        let cellWidth = CalendarView.getCellWidthForViewWithWidth(100.0)
        XCTAssertLessThanOrEqual(cellWidth, 100.0 / 7.0)

        let cellHeight = CalendarView.getCellHeightForViewWithWidth(100.0)
        XCTAssertEqual(cellWidth, cellHeight)
    }

    func testGetHeightForMode() {
        let compactSize = cv.getHeightForMode(usingCompactMode: true)
        let fullSize = cv.getHeightForMode(usingCompactMode: false)
        XCTAssertGreaterThan(fullSize, compactSize)
    }


    func testSelectItem() {
        let date = Date(year: 2020, month: 09, day: 04)
        cv.selectItemWithDate(date, animated: false)


    }
}
