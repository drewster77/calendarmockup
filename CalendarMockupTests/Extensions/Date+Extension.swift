//
//  Date+Extension.swift
//  CalendarMockupTests
//
//  Created on 9/8/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import XCTest
@testable import CalendarMockup

class Date_Extension: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }


    func testInitYMD() {

        let d = Date.init(year: 1980, month: 10, day: 7)
        let comp = Calendar.autoupdatingCurrent.dateComponents([.year, .month, .day], from: d)
        XCTAssertEqual(comp.year, 1980)
        XCTAssertEqual(comp.month, 10)
        XCTAssertEqual(comp.day, 7)
    }

    func testInitYMDHMS() {

        let d = Date.init(year: 1980, month: 10, day: 7, hr: 1, min: 31, sec: 29)
        let comp = Calendar.autoupdatingCurrent.dateComponents([.year, .month, .day, .hour, .minute, .second], from: d)
        XCTAssertEqual(comp.year, 1980)
        XCTAssertEqual(comp.month, 10)
        XCTAssertEqual(comp.day, 7)
        XCTAssertEqual(comp.hour, 1)
        XCTAssertEqual(comp.minute, 31)
        XCTAssertEqual(comp.second, 29)
    }

    func testInitOffsetByWithinSameMonthForward() {
        let d = Date.init(year: 1980, month: 10, day: 7, hr: 1, min: 31, sec: 29).offsetBy(days: 12)
        let comp = Calendar.autoupdatingCurrent.dateComponents([.year, .month, .day, .hour, .minute, .second], from: d)
        XCTAssertEqual(comp.year, 1980)
        XCTAssertEqual(comp.month, 10)
        XCTAssertEqual(comp.day, 19)
        XCTAssertEqual(comp.hour, 1)
        XCTAssertEqual(comp.minute, 31)
        XCTAssertEqual(comp.second, 29)
    }

    func testInitOffsetByWithinSameMonthBackward() {
        let d = Date.init(year: 1980, month: 10, day: 17, hr: 1, min: 31, sec: 29).offsetBy(days: -12)
        let comp = Calendar.autoupdatingCurrent.dateComponents([.year, .month, .day, .hour, .minute, .second], from: d)
        XCTAssertEqual(comp.year, 1980)
        XCTAssertEqual(comp.month, 10)
        XCTAssertEqual(comp.day, 5)
        XCTAssertEqual(comp.hour, 1)
        XCTAssertEqual(comp.minute, 31)
        XCTAssertEqual(comp.second, 29)
    }

    func testInitOffsetByRollingToNextMonth() {
        let d = Date.init(year: 1980, month: 10, day: 7, hr: 1, min: 31, sec: 29).offsetBy(days: 45)
        let comp = Calendar.autoupdatingCurrent.dateComponents([.year, .month, .day, .hour, .minute, .second], from: d)
        XCTAssertEqual(comp.year, 1980)
        XCTAssertNotEqual(comp.month, 10)
        XCTAssertEqual(comp.month, 11)
        XCTAssertEqual(comp.day, 21)
        XCTAssertEqual(comp.hour, 1)
        XCTAssertEqual(comp.minute, 31)
        XCTAssertEqual(comp.second, 29)
    }

    func testInitOffsetByRollingToPriorMonth() {
        let d = Date.init(year: 1980, month: 10, day: 7, hr: 1, min: 31, sec: 29).offsetBy(days: -15)
        let comp = Calendar.autoupdatingCurrent.dateComponents([.year, .month, .day, .hour, .minute, .second], from: d)
        XCTAssertEqual(comp.year, 1980)
        XCTAssertNotEqual(comp.month, 10)
        XCTAssertEqual(comp.month, 9)
        XCTAssertEqual(comp.day, 22)
        XCTAssertEqual(comp.hour, 1)
        XCTAssertEqual(comp.minute, 31)
        XCTAssertEqual(comp.second, 29)
    }

    func testInitEndOfDay() {
        let d = Date.init(year: 2018, month: 5, day: 17, hr: 11, min: 20, sec: 40).endOfDay()
        let comp = Calendar.autoupdatingCurrent.dateComponents([.year, .month, .day, .hour, .minute, .second], from: d)
        XCTAssertEqual(comp.year, 2018)
        XCTAssertEqual(comp.month, 5)
        XCTAssertEqual(comp.day, 17)
        XCTAssertEqual(comp.hour, 23)
        XCTAssertEqual(comp.minute, 59)
        XCTAssertEqual(comp.second, 59)
    }

    func testComparisonOccursOnSameDayAs() {
        let d1 = Date.init(year: 2018, month: 5, day: 17, hr: 11, min: 20, sec: 40)

        let d2 = Date.init(year: 1980, month: 10, day: 7, hr: 1, min: 31, sec: 29)
        XCTAssertFalse(d2.occursOnSameDayAs(d1), "Dates that are very different")

        let d3 = Date.init(year: 2018, month: 5, day: 24, hr: 11, min: 20, sec: 40)
        XCTAssertFalse(d3.occursOnSameDayAs(d1), "Dates 1 week apart")

        let d4 = Date.init(year: 2010, month: 5, day: 17, hr: 11, min: 20, sec: 40)
        XCTAssertFalse(d4.occursOnSameDayAs(d1), "Dates 10 years apart")

        let d5 = Date.init(year: 2018, month: 5, day: 17, hr: 11, min: 20, sec: 40)
        XCTAssertTrue(d5.occursOnSameDayAs(d1), "Dates exactly equal")

        let d6 = Date.init(year: 2018, month: 5, day: 17, hr: 11, min: 20, sec: 41)
        XCTAssertTrue(d6.occursOnSameDayAs(d1), "Dates differing in seconds")

        let d7 = Date.init(year: 2018, month: 5, day: 17, hr: 11, min: 19, sec: 40)
        XCTAssertTrue(d7.occursOnSameDayAs(d1), "Dates differing in minutes")

        let d8 = Date.init(year: 2018, month: 5, day: 17, hr: 11, min: 19, sec: 40)
        XCTAssertTrue(d8.occursOnSameDayAs(d1), "Dates differing in hours")

        let d9 = Date.init(year: 2018, month: 5, day: 17, hr: 0, min: 0, sec: 0)
        XCTAssertTrue(d9.occursOnSameDayAs(d1), "Zero hour/min/sec")

        let d10 = Date.init(year: 2018, month: 5, day: 17, hr: 0, min: 0, sec: 0)
        let d11 = Date.init(year: 2018, month: 5, day: 17, hr: 23, min: 59, sec: 59)
        XCTAssertTrue(d10.occursOnSameDayAs(d11), "Beginning versus end of day")
    }

    func testDateComponents() {
        let d1 = Date.init(year: 2015, month: 10, day: 15, hr: 12, min: 23, sec: 34)
        let comp = d1.dateComponents()
        let realComp = Calendar.autoupdatingCurrent.dateComponents([.year, .month, .day, .hour, .minute, .second, .weekday], from: d1)

        XCTAssertEqual(comp.year, realComp.year)
        XCTAssertEqual(comp.month, realComp.month)
        XCTAssertEqual(comp.day, realComp.day)
        XCTAssertEqual(comp.hour, realComp.hour)
        XCTAssertEqual(comp.minute, realComp.minute)
        XCTAssertEqual(comp.second, realComp.second)
        XCTAssertEqual(comp.weekday, realComp.weekday)
    }

    func testDayOfMonth() {
        let d1 = Date.init(year: 2015, month: 10, day: 15, hr: 12, min: 23, sec: 34)
        let result = d1.dayOfMonth()

        XCTAssertEqual(result, 15)
    }

    func testDayOfWeek() {
        let d1 = Date.init(year: 2015, month: 10, day: 15, hr: 12, min: 23, sec: 34) // Thursday
        let result = d1.dayOfWeek()

        XCTAssertEqual(result, 5)
    }

    func testMonthOfYear() {
        let d1 = Date.init(year: 2015, month: 10, day: 15, hr: 12, min: 23, sec: 34)
        let result = d1.monthOfYear()

        XCTAssertEqual(result, 10)
    }

    func testTimeAsString() {
        let s1 = Date.init(year: 2018, month: 5, day: 4, hr: 17, min: 11, sec: 41).timeAsString
        XCTAssertEqual(s1, "5:11 PM")

        let s2 = Date.init(year: 2018, month: 5, day: 4, hr: 4, min: 23, sec: 41).timeAsString
        XCTAssertEqual(s2, "4:23 AM")

        let s3 = Date.init(year: 2018, month: 5, day: 4, hr: 10, min: 1, sec: 0).timeAsString
        XCTAssertEqual(s3, "10:01 AM")
    }

    func testDateAsStringFull() {
        let s1 = Date.init(year: 2018, month: 5, day: 4, hr: 15, min: 00, sec: 00).dateAsStringFull
        XCTAssertEqual(s1, "Friday, May 4, 2018")

        let s2 = Date.init(year: 2020, month: 12, day: 31, hr: 23, min: 59, sec: 59).dateAsStringFull
        XCTAssertEqual(s2, "Thursday, December 31, 2020")

        let s3 = Date.init(year: 2019, month: 1, day: 1, hr: 0, min: 0, sec: 0).dateAsStringFull
        XCTAssertEqual(s3, "Tuesday, January 1, 2019")
    }
}
