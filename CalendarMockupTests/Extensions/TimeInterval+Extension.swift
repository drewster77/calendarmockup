//
//  TimeInterval+Extension.swift
//  CalendarMockupTests
//
//  Created on 9/8/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import XCTest
@testable import CalendarMockup

class TimeInterval_Extension: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testWholeHoursAndWholeMinutes() {
        let ti = TimeInterval(123456) // 34 hours, 17 minutes, 36 seconds
        XCTAssertEqual(ti.wholeHours, 34)

        let ti2 = ti - (34 * 60 * 60)
        XCTAssertEqual(ti2.wholeMinutes, 17)
    }

    func testAsString() {
        let s = TimeInterval(123456).asString // 34 hours, 17 minutes, 36 seconds
        XCTAssertEqual(s, "34h 17m")

        XCTAssertEqual(TimeInterval(14*60).asString, "14m")
        XCTAssertEqual(TimeInterval(9*60*60).asString, "9h")
    }

}
