//
//  UIImage+Extension.swift
//  CalendarMockupTests
//
//  Created on 9/8/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import XCTest
@testable import CalendarMockup

class UIImage_Extension: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }


    func getPixelColor(image: UIImage, pos: CGPoint) -> UIColor {

        if let pixelData = image.cgImage?.dataProvider?.data {
            let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)

            let pixelInfo: Int = ((Int(image.size.width) * Int(pos.y)) + Int(pos.x)) * 4

            let r = CGFloat(data[pixelInfo+0]) / CGFloat(255.0)
            let g = CGFloat(data[pixelInfo+1]) / CGFloat(255.0)
            let b = CGFloat(data[pixelInfo+2]) / CGFloat(255.0)
            let a = CGFloat(data[pixelInfo+3]) / CGFloat(255.0)

            return UIColor(red: b, green: g, blue: r, alpha: a)
        } else {
            //IF something is wrong I returned WHITE, but change as needed
            return UIColor.white
        }
    }

    func testCircle() {
        let image = UIImage.circle(diameter: 20, color: UIColor.red)

        XCTAssertEqual(image.size, CGSize(width: 20, height: 20), "circle size")
        XCTAssertEqual(getPixelColor(image: image, pos: CGPoint(x: 10, y: 10)), UIColor.red, "Color at center middle")

        // Check color near upper/left -- should be transparent
        let ul = getPixelColor(image: image, pos: CGPoint(x: 2, y: 2))
        let comps = ul.cgColor.components
        XCTAssertNotNil(comps, "color components for color of pixel at upper left")

        if let comps = comps {
            for comp in comps {
                XCTAssertEqual(comp, 0)
            }
        }
    }
    
}
