//
//  AgendaCellLabelTests.swift
//  CalendarMockupTests
//
//  Created on 9/8/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import XCTest
@testable import CalendarMockup

class AgendaCellLabelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    

    func testInit() {
        let label = AgendaCellLabel(size: CGSize(width: 15, height: 25))

        XCTAssertEqual(label.intrinsicContentSize.width, 15)
        XCTAssertEqual(label.intrinsicContentSize.height, 25)
    }
    
}
