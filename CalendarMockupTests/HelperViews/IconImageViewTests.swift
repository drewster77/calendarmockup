//
//  IconImageViewTests.swift
//  CalendarMockupTests
//
//  Created on 9/8/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import XCTest
@testable import CalendarMockup

class IconImageViewTests: XCTestCase {

    var image: UIImage!

    override func setUp() {
        super.setUp()

        image = UIImage.circle(diameter: 50, color: UIColor.red)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    

    func getPixelColor(image: UIImage, pos: CGPoint) -> UIColor {

        if let pixelData = image.cgImage?.dataProvider?.data {
            let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)

            let pixelInfo: Int = ((Int(image.size.width) * Int(pos.y)) + Int(pos.x)) * 4

            let r = CGFloat(data[pixelInfo+0]) / CGFloat(255.0)
            let g = CGFloat(data[pixelInfo+1]) / CGFloat(255.0)
            let b = CGFloat(data[pixelInfo+2]) / CGFloat(255.0)
            let a = CGFloat(data[pixelInfo+3]) / CGFloat(255.0)

            return UIColor(red: b, green: g, blue: r, alpha: a)
        } else {
            //IF something is wrong I returned WHITE, but change as needed
            return UIColor.white
        }
    }

    func testInit() {
        let _ = IconImageView(size: CGSize(width: 40, height: 40), rounded: true, borderColor: UIColor.clear)
        let _ = IconImageView(borderColor: UIColor.blue)
    }
}
