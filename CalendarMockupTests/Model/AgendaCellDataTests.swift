//
//  AgendaCellDataTests.swift
//  CalendarMockupTests
//
//  Created on 9/8/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import XCTest
@testable import CalendarMockup

class AgendaCellDataTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    

    func testInit() {
        let ref = UUID()
        let date = Date()
        let redDot = UIImage.circle(diameter: 12, color: UIColor.red)
        let images = [UIImage.circle(diameter: 32, color: UIColor.blue), UIImage.circle(diameter: 32, color: UIColor.green), UIImage.circle(diameter: 32, color: UIColor.yellow)]
        let data = AgendaCellData(referenceID: ref, startTime: date, length: 3600, dotIcon: redDot, title: "Fire in the hole!", images: images, location: "Wells Fargo")

        XCTAssertEqual(data.referenceID, ref)
        XCTAssertEqual(data.startTime, date)
        XCTAssertEqual(data.length, 3600)
        XCTAssertEqual(data.dotIcon, redDot)
        XCTAssertEqual(data.images, images)
        XCTAssertEqual(data.title, "Fire in the hole!")
        XCTAssertEqual(data.location, "Wells Fargo")
        XCTAssertEqual(data.lengthAsString, "1h")
    }
    
}
