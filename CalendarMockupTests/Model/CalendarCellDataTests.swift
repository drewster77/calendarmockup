//
//  CalendarCellDataTests.swift
//  CalendarMockupTests
//
//  Created on 9/8/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import XCTest
@testable import CalendarMockup

class CalendarCellDataTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInit() {
        let date = Date(year: 2019, month: 04, day: 06, hr: 07, min: 08, sec: 09)
        let d1 = CalendarCellData(startTime: date, dayOfMonth: 6, monthAbbreviation: "Apr", yearText: "2019", showGrayDot: true, isCellBackgroundShaded: false, monthName: "April", dayOfWeek: 6)

        XCTAssertEqual(d1.startTime, date)
        XCTAssertEqual(d1.dayOfMonth, 6)
        XCTAssertEqual(d1.monthAbbreviation, "Apr")
        XCTAssertEqual(d1.monthName, "April")
        XCTAssertEqual(d1.yearText, "2019")
        XCTAssertEqual(d1.showGrayDot, true)
        XCTAssertEqual(d1.isCellBackgroundShaded, false)
        XCTAssertEqual(d1.dayOfWeek, 6)
    }
    
}
