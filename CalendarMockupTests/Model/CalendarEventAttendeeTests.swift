//
//  CalendarEventAttendeeTests.swift
//  CalendarMockupTests
//
//  Created on 9/8/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import XCTest
@testable import CalendarMockup

class CalendarEventAttendeeTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInit() {
        let uuid = UUID()
        let redImage = UIImage.circle(diameter: 32, color: UIColor.red)
        let blueImage = UIImage.circle(diameter: 32, color: UIColor.blue)
        let att = CalendarEventAttendee(id: uuid, firstName: "Bill", lastName: "Clinton", image: redImage)

        XCTAssertEqual(att.id, uuid)
        XCTAssertEqual(att.firstName, "Bill")
        XCTAssertEqual(att.lastName, "Clinton")
        XCTAssertEqual(att.fullName, "Bill Clinton")
        XCTAssertEqual(att.image, redImage)
        XCTAssertNotEqual(att.image, blueImage)
    }
    
}
