//
//  CalendarEventCategoryTests.swift
//  CalendarMockupTests
//
//  Created on 9/8/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import XCTest
@testable import CalendarMockup


class CalendarEventCategoryTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInit() {
        let catMeeting = CalendarEventCategory.meeting
        let catMeetingIcon = catMeeting.icon

        let catParty = CalendarEventCategory.party
        let catPartyIcon = catParty.icon

        XCTAssertEqual(catMeeting, CalendarEventCategory.meeting)
        XCTAssertEqual(catParty, CalendarEventCategory.party)
        XCTAssertNotEqual(catMeetingIcon, catPartyIcon)
    }
    
}
