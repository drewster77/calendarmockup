//
//  CalendarEventTest.swift
//  CalendarMockupTests
//
//  Created on 9/8/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import XCTest
@testable import CalendarMockup

class CalendarEventTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testInitializer() {
        let uuid = UUID(uuidString: "6904850E-88F6-4E02-BA75-64D65E4D1B04")!
        let startTime = Date(year: 2018, month: 12, day: 13, hr: 14, min: 15, sec: 16)
        let ce = CalendarEvent(id: uuid, title: "Take a shower", description: "Whatever", location: "Jerry Seinfeld's house", startTime: startTime, length: TimeInterval(150*60), attendees: [CalendarEventAttendee](), category: CalendarEventCategory.meeting)

        XCTAssertEqual(ce.category, CalendarEventCategory.meeting)
        XCTAssertEqual(ce.title, "Take a shower")
        XCTAssertEqual(ce.description, "Whatever")
        XCTAssertEqual(ce.location, "Jerry Seinfeld's house")
        XCTAssertEqual(ce.startTime, startTime)
        XCTAssertEqual(ce.attendees.count, 0)
        XCTAssertEqual(ce.length, 150*60)
        XCTAssertEqual(ce.endTime, Date(year: 2018, month: 12, day: 13, hr: 16, min: 45, sec: 16))
    }
    
}
