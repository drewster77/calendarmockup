//
//  DayOfWeekTests.swift
//  CalendarMockupTests
//
//  Created on 9/8/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import XCTest
@testable import CalendarMockup

class DayOfWeekTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testDayOfWeek() {
        XCTAssertEqual(DayOfWeek(rawValue: 3), DayOfWeek.wednesday)
        XCTAssertEqual(DayOfWeek.sunday.rawValue, 0)

        XCTAssertEqual(DayOfWeek.sunday.abbreviation, "S")
        XCTAssertEqual(DayOfWeek.monday.abbreviation, "M")
        XCTAssertEqual(DayOfWeek.tuesday.abbreviation, "T")
        XCTAssertEqual(DayOfWeek.wednesday.abbreviation, "W")
        XCTAssertEqual(DayOfWeek.thursday.abbreviation, "T")
        XCTAssertEqual(DayOfWeek.friday.abbreviation, "F")
        XCTAssertEqual(DayOfWeek.saturday.abbreviation, "S")
    }

    func testDayOfWeek2() {
        let dow = DayOfWeek(rawValue: 4)

        XCTAssertNotNil(dow)
        XCTAssertEqual(dow?.abbreviation, "T")

        let dow2 = DayOfWeek.friday
        XCTAssertNotNil(dow2)
        XCTAssertEqual(dow2.abbreviation, "F")
        XCTAssertEqual(dow2.rawValue, 5)

        let dow3 = DayOfWeek(rawValue: 7)
        XCTAssertNil(dow3)
    }
    
}
