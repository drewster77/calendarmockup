//
//  MonthOfTheYearTests.swift
//  CalendarMockupTests
//
//  Created on 9/8/18.
//  Copyright © 2018 CalendarMockup. All rights reserved.
//

import XCTest
@testable import CalendarMockup

class MonthOfTheYearTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    

    func testMonthOfTheYearInit() {
        let moyJan = MonthOfTheYear(rawValue: 1)
        XCTAssertEqual(moyJan, MonthOfTheYear.january)

        let moy12 = MonthOfTheYear.december
        XCTAssertEqual(moy12.rawValue, 12)

        XCTAssertNil(MonthOfTheYear(rawValue: 0), "Month zero")
        XCTAssertNil(MonthOfTheYear(rawValue: 13), "Month 13")
        XCTAssertNil(MonthOfTheYear(rawValue: -1), "Month negative")
    }

    func testMonthOfTheYearName() {
        XCTAssertEqual(MonthOfTheYear.january.name, "January")
        XCTAssertEqual(MonthOfTheYear.february.name, "February")
        XCTAssertEqual(MonthOfTheYear.march.name, "March")
        XCTAssertEqual(MonthOfTheYear.april.name, "April")
        XCTAssertEqual(MonthOfTheYear.may.name, "May")
        XCTAssertEqual(MonthOfTheYear.june.name, "June")
        XCTAssertEqual(MonthOfTheYear.july.name, "July")
        XCTAssertEqual(MonthOfTheYear.august.name, "August")
        XCTAssertEqual(MonthOfTheYear.september.name, "September")
        XCTAssertEqual(MonthOfTheYear.october.name, "October")
        XCTAssertEqual(MonthOfTheYear.november.name, "November")
        XCTAssertEqual(MonthOfTheYear.december.name, "December")

    }

    func testMonthOfTheYearAbbreviation() {
        XCTAssertEqual(MonthOfTheYear.january.abbreviation, "Jan")
        XCTAssertEqual(MonthOfTheYear.february.abbreviation, "Feb")
        XCTAssertEqual(MonthOfTheYear.march.abbreviation, "Mar")
        XCTAssertEqual(MonthOfTheYear.april.abbreviation, "Apr")
        XCTAssertEqual(MonthOfTheYear.may.abbreviation, "May")
        XCTAssertEqual(MonthOfTheYear.june.abbreviation, "Jun")
        XCTAssertEqual(MonthOfTheYear.july.abbreviation, "Jul")
        XCTAssertEqual(MonthOfTheYear.august.abbreviation, "Aug")
        XCTAssertEqual(MonthOfTheYear.september.abbreviation, "Sep")
        XCTAssertEqual(MonthOfTheYear.october.abbreviation, "Oct")
        XCTAssertEqual(MonthOfTheYear.november.abbreviation, "Nov")
        XCTAssertEqual(MonthOfTheYear.december.abbreviation, "Dec")

    }
}
