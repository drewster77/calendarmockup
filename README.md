**CalendarMockup**

This was developed as an example of setting up iOS view layouts programmatically, rather than by using Storyboards or XIBs, etc..

In this case, the app is a limited-functionality clone of Microsoft's Outlook app for iOS.

